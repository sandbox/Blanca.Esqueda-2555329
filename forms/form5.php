<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
    <p>An applicant.</p>
    <h2>Purpose</h2>
    <p>To start a dispute proceeding before the Agency.</p>
    <h2>When should you file this form?</h2>
    <p>You are expected to have tried to resolve the dispute with the respondent yourself before filing an application with the Agency. In some instances, the Agency will not consider your application until it is satisfied that you have taken this step.</p>
    <p>You should file your application as soon as possible after failing to resolve your dispute with the respondent to reduce the risk that relevant documents are not kept by all parties and to ensure that all options for relief are available to you.</p>
    <h2>What happens next?</h2>
    <p>You will be informed if your application is complete and, if so, the start date of the proceeding.</p>
    <p>If your application is incomplete, you will be notified in writing and you must provide the missing information within 20 business days after the date of the notice. If you fail to provide the missing information within the time limit, your file will be closed.</p>
    <p>Refer to <a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-1" target="_blank">section 18</a> of the Dispute Adjudication Rules for more information.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
    <p>Un demandeur.</p>
    <h2>But</h2>
    <p>Entamer une instance de règlement d’un différend devant l’Office.&nbsp;</p>
    <h2>Quand devriez-vous déposer ce formulaire?</h2>
    <p>Avant de déposer une demande auprès de l’Office, vous devez avoir tenté de régler vous-même le différend avec le défendeur. Dans certains cas, l’Office ne traitera pas votre demande à moins d’être convaincu que vous avez agi en ce sens.<strong></strong></p>
    <p>Si vous ne parvenez pas à régler votre différend avec le défendeur, vous devriez déposer votre demande dans les plus brefs délais. Ainsi, vous réduirez le risque que toutes les parties concernées ne conservent pas les documents pertinents et vous vous assurerez de n’éliminer aucune option de redressement.<strong></strong></p>
    <h2>Quelle est la prochaine étape?</h2>
    <p>Si votre demande est complète, vous en serez informé et, le cas échéant, vous connaîtrez la date du début de l'instance.</p>
    <p>Si votre demande est incomplète, vous en serez avisé par écrit et vous disposerez de vingt jours ouvrables suivant la date de l’avis pour la compléter. Si vous ne la complétez pas dans le délai imparti, votre dossier sera fermé. &nbsp;</p>
    <p>Veuillez consulter l’<a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-1\" target=\"_blank\">article 18</a> des Règles pour le règlement des différends pour de plus amples renseignements.&nbsp;</p>";


  $person_filing_form_options = array(
    '1' => t("I'm filing on behalf of myself"),
    '2' => t("I'm filing on behalf myself and other individuals"),
    '3' => t("I'm filing on behalf one or more organizations"),
    '4' => t("I'm a lawyer"),
    '5' => t("I'm a family member, friend, etc, who is representing the applicant(s)")
  );
  
  $person_filing_form_options_fr = array(
    '1' => "Je dépose le formulaire en mon nom.",
    '2' => "Je dépose le formulaire en mon nom et au nom d'autres personnes.",
    '3' => "Je dépose le formulaire au nom d'un ou de plusieurs organismes.",
    '4' => "Je suis un avocat.",
    '5' => "Je suis un membre de la famille, un ami, etc., qui représente le ou les demandeurs.",
  );
  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 2: Contact Information'), 
      '@L[fr]#markup' => 'Partie 1 de 2 : Coordonnées',
    ),
    
    /*
    'attention_text' => array(
      '@extends' => 'attention_text',
      '#markup' => '<span class="important-white-icon-l">Note: you must not raise any new issues, arguments or introduce new evidence that was not discussed in the response</span>',
      '@L[fr]#markup' => '',
    ),
     * 
     */
    
    // The fieldset for the first radio button select box
    'fieldset_person_filing_form' => array(
      '#type' => 'fieldset',
      '#title' => t('Who is filing this form?'),
      // Translates the title attribute when in french
      '@L[fr]#title' => 'Qui dépose ce formulaire?',
      
      
      // The first radio button select box
      'person_filing_form' => array(
        '#title' => '',
        '@L[fr]#title' => '',
        '#type' => 'radios',
        '#options' => $person_filing_form_options,
        '@L[fr]#options' => $person_filing_form_options_fr,
        '#required' => TRUE,
        // Send the result of this field via ajax
        '@ajax_send' => TRUE,
        '#limit_validation_errors' => array(),
        '#ajax' => array(
          'build_action' => 'dynamic_forms_variable_set',
          'build_action_args' => array(
            array(
              'variable' => 'applicant_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'representative_contact_copies',
              'value' => 1,
            ),
            array(
              'variable' => 'organization_contact_copies',
              'value' => 1,
            ),
          ),
        ),
      ),
     ),
    
    
    'how_to_fill_multi' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Applicant" section below.
                     <br>Add the contact information for the other applicant(s) by clicking the "Add another applicant" button.',
        '@L[fr]#markup' => 'Fournissez vos coordonnées dans la section « Demandeur » ci-dessous. Ajoutez les coordonnées de tout autre demandeur en cliquant sur le bouton « Ajouter un autre demandeur ».',
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '2',
      ),
    ),
    
    'how_to_fill_org' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'If you are filing this application on behalf of a company, an association or other organization, identify yourself and provide the contact information for the organization.',
        '@L[fr]#markup' => "Si vous déposez cette demande au nom d'une entreprise, d'une association ou d'un autre organisme, identifiez-vous et fournissez les coordonnées de l'organisme.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '3',
      ),
    ),
    
    'how_to_fill_fam' => array(
      '@extends' => 'info_text2',
      
      'heading' => array(
        '#markup' => 'How to fill in the contact information',
        '@L[fr]#markup' => 'Comment remplir la section des coordonnées',
      ),
      'body' => array(
        '#markup' => 'Fill out your contact information in the "Representative" section.
                    <br>Add the contact information for whoever you\'re representing in the "Applicant" section below.',
        '@L[fr]#markup' => "Fournissez vos coordonnées dans la section « Représentant ». Ajoutez les coordonnées de toute personne dont vous êtes le représentant dans la section « Demandeur » ci-dessous.",
      ),
      '@dependencies' => array(
        'value1' => '{person_filing_form}',
        'operator' => '==',
        'value2' => '5',
      ),
    ),
    
    'representatives' => array(
      '@extends' => 'representative_contact_group',
    ),
    
    'organizations' => array(
      '@extends' => 'organization_contact_group',
    ),
    
    'lawyer' => array(
      '@extends' => 'lawyer_contact_group',
    ),
    
    'applicants' => array(
      'contact_info' => array(
        'consent' => array(
          '@extends' => 'description_text',
          '#markup' => "<p>We need the applicant's consent for you to act on their behalf. We will send you a pre-filled consent form automatically, once you submit this form. The Agency will not be able to process the application until it receives the signed consent form.</p>",
          '@L[fr]#markup' => "<p>La déclaration du demandeur qui vous autorise à agir en son nom doit nous être fournie. Nous vous ferons automatiquement parvenir un formulaire de consentement prérempli, une fois que vous aurez soumis ce formulaire. L'Office ne sera pas en mesure de traiter la demande avant d'avoir reçu le formulaire de consentement signé.</p>",
          '@dependencies' => array(
            'value1' => '{person_filing_form}',
            'operator' => '==',
            'value2' => '5',
          ),
          '#weight' => -10,
        ),
        
        'same_as' => array(
          '@extends' => 'same_as',
          '#ajax' => array(
            'render_action' => 'dynamic_forms_same_as',
            'render_action_args' => array(
              'applicant_street_address#' => 'representative_street_address',
              'applicant_city#' => 'representative_city',
              'applicant_province#' => 'representative_province',
              'applicant_postal_code#' => 'representative_postal_code',
              'applicant_country#' => 'representative_country',
            ),
          ),
        ),
      ),
      '@extends' => 'applicant_contact_group',
    ),
    
    'respondents' => array(
      '@extends' => 'respondent_contact_group',
    ),
    
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'application',
      '<form_short_name_fr>' => 'demande',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '#markup' => t('Part 2 of 2: Details of the Application'),
      '@L[fr]#markup' => 'Partie 2 de 2 : Détails relatifs au demande',
    ),
    
    'full_description' => array(
      '#type' => 'textarea',
      '#title' => t('Provide a full description of the facts.'),
      '@L[fr]#title' => 'Donnez une description complète des faits.',
      '#required' => TRUE,
    ),
    
    'state_issues' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly state the issues.'),
      '@L[fr]#title' => 'Énoncez clairement les questions en litige.',
      '#required' => TRUE,
    ),
    
    'legislative_provisions' => array(
      '#type' => 'textarea',
      '#title' => t('Identify any legislative provisions on which you are relying.'),
      '@L[fr]#title' => 'Indiquez toutes les dispositions législatives sur lesquelles vous fondez votre demande.',
      '#required' => TRUE,
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the arguments in support of your application.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre demande.",
      '#required' => TRUE,
    ),
    
    'relief_seeking' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly set out the relief you are seeking.'),
      '@L[fr]#title' => 'Indiquez clairement la réparation demandée.',
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your application, you must file them on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre demande, vous devez les déposer le même jour que votre demande.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
    'personal_info' => array(
      '@extends' => 'personal_info',
    )
  );
    
  
  $formArray = array(
    //'@overrides' => array(
    //  'organization->#weight' => NULL,
    //)
    '@post_submission' => array(
      '@extends' => 'post_submission',
    )
  );
  
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;

  return $formArray;
}

/**
 * Check requirements
 */
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    $field_value = get_value('{person_filing_form}');

    if ($field_value == 4 OR $field_value == 5) {
        
      /*
      $contact_elements = array(
        'applicant_first_name',
        'applicant_last_name',
        'applicant_street_address',
        'applicant_city',
        'applicant_province',
        'applicant_postal_code',
        'applicant_country',
        'applicant_email',
        'applicant_phone_number',
      );

      foreach ($contact_elements as $contact) {
        if (strpos($field, $contact) !== FALSE) {
          return TRUE;
        }
      }
       */
      if (strpos($field, 'applicant_email') !== FALSE) {
        return FALSE;
      }
    }
  }
}


function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
    
    // Get value of person filling form if available
    $field_value = get_value('{person_filing_form}');
      
    if ($field == 'applicants') {
      // Evaluate result
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value != 3);
      }
    }   
    elseif ($field == 'organizations') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 3);
      }
    }
    elseif ($field == 'lawyer') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 4);
      }
    }
    elseif ($field == 'representatives') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return ($field_value == 5);
      }
    }
    elseif ($field == 'respondents') {
      // Dependency logic
      if (!isset($field_value)) {
        return FALSE;
      }
      else {
        return TRUE;
      }
    }    
    elseif ($field == 'applicant_add_button') {
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2 OR $field_value == 4 OR $field_value == 5);
      }
      else {
        return FALSE;
      }
    }
    elseif ($field == 'applicant_remove_button') {
      if (isset($result) AND $result === FALSE) {
        return FALSE;
      }
      // Dependency logic
      if (isset($field_value)) {
        return ($field_value == 2 OR $field_value == 4 OR $field_value == 5);
      }
      else {
        return FALSE;
      }
    }
    elseif (strpos($field, 'applicant_organization') !== FALSE) {
      if ($field_value == 4 OR $field_value == 5) {
        return TRUE;
      }
    }
    elseif (strpos($field, 'applicant_same_as') !== FALSE) {
      if (isset($field_value)) {
        return ($field_value == 5);
      }
    }
    elseif (strpos($field, 'respondent_info_text') !== FALSE) {
      return TRUE;      
    }
  }
}

