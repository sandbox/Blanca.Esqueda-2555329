<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person who is filing a request for confidentiality in respect of a document that they are filing.</p>
<h2>Purpose</h2>
<p>To request that documents filed with the Agency be treated confidentially and not be placed on the public record.</p>
<h2>When should you file this form?</h2>
<p>At the same time as you file a document with the Agency for which you are claiming confidentiality.</p>
<h2>What happens next?</h2>
<p>A party may oppose your request for confidentiality within 5 business days after the day on which they receive it by filing a request for disclosure. You may respond to any request for disclosure in accordance with <a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">subsection 31(4)</a> (<a href="https://services.cta-otc.gc.ca/forms" target="_blank">Form 14</a>) within 3 business days after the day on which you receive the request for disclosure.</p>
<p>The Agency will consider whether to grant the request after reviewing all the submissions of the parties.</p>
<p>If the Agency denies your request for confidentiality, your document will be placed on the public record.</p>
<p>If your request is granted, the Agency may order that a part or the whole document be placed on the confidential record or be provided to the other parties on the basis that it be kept strictly confidential.</p>
<p>Refer to <a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">section 31</a> of the Dispute Adjudication Rules for more information.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://23.23.154.142/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>
</div>
<div class="clear"></div>
<div class="gen-box-did-you-know span-5">
<h2><span class="did-you-know-outline-icon-l"></span>What do I need before I begin?</h2>
<p>You will need to provide:</p>
<ul>
<li>One public version of the document from which the confidential information has been redacted, which is sent to all parties; and</li>
<li>One confidential version of the document , which:
<ul>
<li>contains and identifies the confidential information that was redacted from the public version; and</li>
<li>states “CONTAINS CONFIDENTIAL INFORMATION” on the top of each page.</li>
</ul>
</li>
</ul>
<p>&nbsp;The confidential version is only filed with the Agency at this stage in the proceedings.</p>';

  $intro_text_fr = "<<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne qui dépose une requête de confidentialité à l'égard d'un document qu’elle dépose.&nbsp;</p>
<h2>But</h2>
<p>Demander que les documents déposés auprès de l’Office soient considérés confidentiels et ne soient pas versés aux archives publiques.&nbsp;</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>En même temps que vous déposez auprès de l’Office un document qui fait l’objet d’une requête de confidentialité.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>Une partie qui souhaite s’opposer à votre requête de confidentialité dépose une requête de communication dans les cinq jours ouvrables suivant la date de réception de la copie de la requête de confidentialité. Vous pouvez répondre à une requête de communication en vertu du <a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">paragraphe 31(4)</a> (<a href=\"https://services.cta-otc.gc.ca/fra/formulaires\" target=\"_blank\">formulaire 14</a>) dans les trois jours ouvrables suivant la date de réception de la copie de la requête de communication. <strong></strong></p>
<p>Après avoir examiné toutes les présentations des parties, l’Office décidera d’accorder ou non la requête. &nbsp;</p>
<p>Si l’Office rejette votre requête de confidentialité, votre document sera versé aux archives publiques. &nbsp;</p>
<p>Si votre requête est accordée, l’Office peut exiger que tout le document, ou une partie de celui-ci, soit versé aux archives confidentielles ou qu’il soit fourni aux autres parties à la condition que sa confidentialité soit préservée.</p>
<p>Veuillez consulter l’<a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\">article 31</a>&nbsp;des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://23.23.154.142/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>
</div>
<div class=\"clear\"></div>
<div class=\"gen-box-did-you-know span-5\">
<h2><span class=\"did-you-know-outline-icon-l\"></span><strong>De quoi ai-je besoin avant de commencer?&nbsp;</strong></h2>
<p>Vous devrez fournir :&nbsp;</p>
<ul>
<li>une version <strong>publique</strong> du document, de laquelle les renseignements confidentiels ont été supprimés, laquelle version est envoyée à toutes les parties;</li>
<li>une version <strong>confidentielle </strong>du document qui&nbsp;:
<ul>
<li>contient et indique les passages qui ont été supprimés de la version publique;</li>
<li>porte la mention « CONTIENT DES RENSEIGNEMENTS CONFIDENTIELS » au haut de chaque page.</li>
</ul>
</li>
</ul>
<p>Ce n'est qu'à cette étape de l'instance que la version confidentielle est déposée auprès de l’Office.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
    
    'up_to_date_contact_info' => array(
      '@extends' => 'up_to_date_contact_info',
    ),
    
    
  );




  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'request for confidentiality',
      '<form_short_name_fr>' => 'request for confidentiality',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs au <form_short_name_fr>',
    ),
    
    'ident_doc' => array(
      '#type' => 'textarea',
      '#title' => t('Identify the document or portion of the document that contains confidential information.'),
      '@L[fr]#title' => 'Indiquez le document ou la partie du document qui contient des renseignements confidentiels.',
      '#required' => TRUE,
    ),
    
    'list_parties' => array(
      '#type' => 'textarea',
      '#title' => t('List the parties, if any, with whom you would be willing to share the document.'),
      '@L[fr]#title' => 'Dressez, le cas échéant, la liste des parties avec lesquelles vous seriez disposé à partager le document.',
      '#required' => TRUE,
    ),
    
    'reasons_support' => array(
      '#type' => 'textarea',
      '#title' => t('Set out in detail the reasons in support of your request for confidentiality, including an explanation of the relevance to the dispute proceeding and any specific direct harm that might result from its disclosure.'),
      '@L[fr]#title' => "Détaillez les motifs à l'appui de votre requête de confidentialité, y compris une explication de la pertinence du document à l'instance et la description du préjudice direct précis qui pourrait résulter de la divulgation du document.",
      '#required' => TRUE,
    ),
    
    'list_docs' => array(
      '#type' => 'textarea',
      '#title' => t('List the documents that you will be filing.'),
      '@L[fr]#title' => 'Dressez la liste des documents que vous déposerez.',
      '#required' => TRUE,
    ),
    
    'how_do_i_file_my_docs' => array(
      '@extends' => 'question_text2',
      'heading' => array(
        '#markup' => 'How do I file my documents?',
        '@L[fr]#markup' => "Comment dois-je déposer mes documents?",
      ),
      'body' => array(
        '#markup' => 'After you submit the form, you will be emailed a link to a secure file transfer system. You will have an account to manage your documents.</p>'
        . '<p>Please upload your files right away.</p>'
        . '<p>You can also file documents by fax, courier, or personal delivery.',
        '@L[fr]#markup' => "Une fois que vous aurez déposé le formulaire, vous recevrez par courriel un lien vers un système sécurisé de transfert de fichiers. Vous aurez un compte qui vous permettra de gérer vos documents.</p>"
        . "<p>Veuillez télécharger vos fichiers immédiatement.</p>"
        . "<p>Vous avez aussi la possibilité de déposer des documents par télécopieur, par service de messagerie, ou en main propre.",
      ),
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your request, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre requête, vous devez les déposer le même jour que votre requête.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

