<?php

// Initial Delcation

namespace DF;

class templates {

  public static $field_templates = array();

}

/**
 * Put templates below
 */

templates::$field_templates['df_hidden_fix'] = array(
  '#type' => 'textfield',
  '#ajax' => array(
    'callback' => 'dynamic_forms_generator_callback'
  ),
);

templates::$field_templates['prev'] = array(
  '#type' => 'submit',
  '#value' => t('Previous Page'),
  '@L[fr]#value' => 'Page précédente',
  '#attributes' => array('class' => array('df-prev')),
  '#executes_submit_callback' => FALSE,
  '#limit_validation_errors' => array(),
  '#name' => 'prev',
);

templates::$field_templates['next'] = array(
  '#type' => 'submit',
  '#value' => t('Next Page'),
  '@L[fr]#value' => 'Page suivante',
  '#attributes' => array('class' => array('df-next')),
  '#executes_submit_callback' => FALSE,
  '#name' => 'next',
);

templates::$field_templates['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Submit'),
  '@L[fr]#value' => 'Soumettre',
  '#attributes' => array('id' => array('edit-submit')),  
  '#executes_submit_callback' => TRUE,
  //'#submit' => array('dynamic_forms_submit'),
);


templates::$field_templates['description_text'] = array(
  '#markup' => '',
  '@L[fr]#markup' => '',
  '#prefix' => '<div class="gen-box-2"><p>',
  '#suffix' => '</p></div>',
);

templates::$field_templates['info_text'] = array(
  '#markup' => '',
  '@L[fr]#markup' => '',
  '#prefix' => '<div class="gen-box-information">',
  '#suffix' => '</div>',
);

templates::$field_templates['info_text2'] = array(
  '#type' => 'group',
  '#prefix' => '<div class="gen-box-information">',
  '#suffix' => '</div>',
  
  'heading' => array(
    '#markup' => 'ADD HEADING HERE',
    '#prefix' => '<h2><span class="information-outline-icon-l">',
    '#suffix' => '</span></h2>',
  ),
  
  'body' => array(
    '#markup' => 'ADD BODY TEXT HERE',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ),
);


templates::$field_templates['question_text'] = array(
  '#markup' => '',
  '@L[fr]#markup' => '',
  '#prefix' => '<div class="gen-box-did-you-know">',
  '#suffix' => '</div>', 
);

templates::$field_templates['question_text2'] = array(
  '#type' => 'group',
  '#prefix' => '<div class="gen-box-did-you-know">',
  '#suffix' => '</div>',
  
  'heading' => array(
    '#markup' => 'ADD HEADING HERE',
    '#prefix' => '<h2><span class="did-you-know-outline-icon-l">',
    '#suffix' => '</span></h2>',
    '@wrapper_tag' => FALSE,
  ),
  
  'body' => array(
    '#markup' => 'ADD BODY TEXT HERE',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ),
);

templates::$field_templates['attention_text'] = array(
  '#markup' => '',
  '@L[fr]#markup' => '',
  '#prefix' => '<div class="gen-box-important">',
  '#suffix' => '</div>', 
);

templates::$field_templates['attention_text2'] = array(
  '#type' => 'group',
  '#prefix' => '<div class="gen-box-important">',
  '#suffix' => '</div>',
  
  'heading' => array(
    '#markup' => 'ADD HEADING HERE',
    '#prefix' => '<h2><span class="important-white-icon-l">',
    '#suffix' => '</span></h2>',
  ),
  
  'body' => array(
    '#markup' => 'ADD BODY TEXT HERE',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ),
);

templates::$field_templates['page_heading'] = array(
  '#markup' => t('Part ...'),
  '@L[fr]#markup' => '',
  '#prefix' => '<div class="df-page-heading"><h2>',
  '#suffix' => '</h2></div>',
);


templates::$field_templates['remove_button'] = array(
  '#type' => 'button',
  '#button_type' => 'button',
  '#value' => t('Remove'),
  '@L[fr]#value' => 'Supprimer',
  '#attributes' => array('class' => array('df-remove-button')),
  '@ajax_send' => TRUE,
  '#limit_validation_errors' => array(),
  '#name' => 'add_botton',  // !!! Make sure to make this unique if you want multiple buttons
);

templates::$field_templates['add_button'] = array(
  '#type' => 'button',
  '#button_type' => 'button',
  '#value' => t('Add'),
  '@L[fr]#value' => 'Supprimer',
  '#attributes' => array('class' => array('df-add-button')),
  '@ajax_send' => TRUE,  
  '#limit_validation_errors' => array(),
  '#name' => 'add_botton',  // !!! Make sure to make this unique if you want multiple buttons
);


templates::$field_templates['email'] = array(
  '#title' => t('Email address'),
  '@L[fr]#title' => "Adresse électronique",
  '#type' => 'textfield',
  '#rules' => array(
    array('rule' => 'email', 'error' => t('Please enter a valid email address.')),
  //'length[10, 50]',
  //array('rule' => 'alpha_numeric', 'error' => 'Please, use only alpha numeric characters at {field}.'),
  //array('rule' => 'match_field[otherfield]', 'error callback' => 'mymodule_validation_error_msg'),
  ),
  '#filters' => array('trim', 'lowercase')
);

templates::$field_templates['name'] = array(
  '#title' => '',
  '@L[fr]#title' => '',
  '#type' => 'textfield',
  '#rules' => array(
    array('rule' => 'length[2, 200]', 'error' => t('Please use full name')),
  ),
);

templates::$field_templates['postal_code'] = array(
  '#title' => t('Postal/Zip code'),
  '@L[fr]#title' => 'Code postal/code de zone',
  '#type' => 'textfield',
  '#filters' => array('trim', 'uppercase'),
);

templates::$field_templates['fax'] = array(
  '#title' => t('Fax'),
  '@L[fr]#title' => 'Télécopieur',
  '#type' => 'textfield',
  '#size' => '20',
);

templates::$field_templates['phone_number'] = array(
  '#title' => t('Phone'),
  '@L[fr]#title' => 'Téléphone',
  '#type' => 'textfield',
  '#size' => '20',
);

templates::$field_templates['phone_number_ext'] = array(
  '#title' => t('Extension'),
  '@L[fr]#title' => 'Poste',
  '#type' => 'textfield',
  '#size' => '7',
);

templates::$field_templates['phone_and_extension'] = array(
  '#title' => t('Telephone number'),
  '@L[fr]#title' => 'Numéro de téléphone',
  '#type' => 'fieldset',
  
  'phone_number' => array(
    '@extends' => 'phone_number',
  ),
  'phone_number_ext' => array(
    '@extends' => 'phone_number_ext',
  ),
);

templates::$field_templates['fieldset_radio'] = array(
  '#type' => 'fieldset',
  '#title' => '',
  '@L[fr]#title' => '',
  
  '[radio_name]' => array(
    '#title' => '',
    '@L[fr]#title' => '',
    '#type' => 'radios',
    '#options' => array(),    
  ),
);

templates::$field_templates['provinces_options'] = array(
  'Alberta' => 'Alberta',
  'British Columbia' => 'British Columbia',
  'Manitoba' => 'Manitoba',
  'New Brunswick' => 'New Brunswick',
  'Newfoundland and Labrador' => 'Newfoundland and Labrador',
  'Northwest Territories' => 'Northwest Territories',
  'Nova Scotia' => 'Nova Scotia',
  'Nunavut Territory' => 'Nunavut Territory',
  'Ontario' => 'Ontario',
  'Prince Edward Island' => 'Prince Edward Island',
  'Quebec' => 'Quebec',
  'Saskatchewan' => 'Saskatchewan',
  'Yukon Territory' => 'Yukon Territory',
  'Alaska' => 'Alaska',
  'Alabama' => 'Alabama',
  'Arkansas' => 'Arkansas',
  'American Samoa' => 'American Samoa',
  'Arizona' => 'Arizona',
  'California' => 'California',
  'Colorado' => 'Colorado',
  'Connecticut' => 'Connecticut',
  'District of Columbia' => 'District of Columbia',
  'Delaware' => 'Delaware',
  'Florida' => 'Florida',
  'Georgia' => 'Georgia',
  'Guam' => 'Guam',
  'Hawaii' => 'Hawaii',
  'Iowa' => 'Iowa',
  'Idaho' => 'Idaho',
  'Illinois' => 'Illinois',
  'Indiana' => 'Indiana',
  'Kansas' => 'Kansas',
  'Kentucky' => 'Kentucky',
  'Louisiana' => 'Louisiana',
  'Massachusetts' => 'Massachusetts',
  'Maryland' => 'Maryland',
  'Maine' => 'Maine',
  'Michigan' => 'Michigan',
  'Minnesota' => 'Minnesota',
  'Missouri' => 'Missouri',
  'Mississippi' => 'Mississippi',
  'Montana' => 'Montana',
  'North Carolina' => 'North Carolina',
  'North Dakota' => 'North Dakota',
  'Nebraska' => 'Nebraska',
  'New Hampshire' => 'New Hampshire',
  'New Jersey' => 'New Jersey',
  'New Mexico' => 'New Mexico',
  'Nevada' => 'Nevada',
  'New York' => 'New York',
  'Ohio' => 'Ohio',
  'Oklahoma' => 'Oklahoma',
  'Oregon' => 'Oregon',
  'Pennsylvania' => 'Pennsylvania',
  'Puerto Rico' => 'Puerto Rico',
  'Palau' => 'Palau',
  'Rhode Island' => 'Rhode Island',
  'South Carolina' => 'South Carolina',
  'South Dakota' => 'South Dakota',
  'Tennessee' => 'Tennessee',
  'Texas' => 'Texas',
  'Utah' => 'Utah',
  'Virginia' => 'Virginia',
  'Virgin Islands' => 'Virgin Islands',
  'Vermont' => 'Vermont',
  'Washington' => 'Washington',
  'Wisconsin' => 'Wisconsin',
  'West Virginia' => 'West Virginia',
  'Wyoming' => 'Wyoming',
);

templates::$field_templates['province'] = array(
  '#title' => t('Province/Territory/State'),
  '@L[fr]#title' => 'Province/territoire/état',
  '#type' => 'select',
  '#options' => templates::$field_templates['provinces_options'],
);

templates::$field_templates['country_options'] = array(
  'CA' => 'Canada',
  'US' => 'United States',
);

templates::$field_templates['country'] = array(
  '#title' => t('Country'),
  '@L[fr]#title' => 'Pays',
  '#type' => 'select',
  '#options' => templates::$field_templates['country_options'],
);


templates::$field_templates['same_as'] = array(
  '#title' => t('Same address as representative'),
  '@L[fr]#title' => 'Même adresse que celle du représentant',
  '#type' => 'checkbox',
  '@ajax_send' => TRUE,
  /*
  '#ajax' => array(
    'build_action' => 'dynamic_forms_same_as',
    'build_action_args' => array(
      'statement' => array(
        'respondent_street_address<respondent_contact_copies>' => 'representative_street_address',
        'respondent_city<respondent_contact_copies>' => 'representative_city',
        'respondent_province<respondent_contact_copies>' => 'representative_province',
        'respondent_postal_code<respondent_contact_copies>' => 'representative_postal_code',
        'respondent_country<respondent_contact_copies>' => 'representative_country',
      ),
    ),
  ),
   * 
   */
);

templates::$field_templates['basic_contact_info'] = array(  
  '#type' => 'fieldset',
  '#title' => 'Contact information',
  '@L[fr]#title' => 'Coordonnées',
  
  'who_is_filing' => array(
    '#markup' => '<b>Who is filing this form?</b>',
    '@L[fr]#markup' => '<b>Qui dépose ce formulaire?</b>',
  ),
  
  'first_name' => array(
    '#title' => 'First name',
    '@L[fr]#title' => 'Prénom',
    '@extends' => 'name',
    '#required' => TRUE,
  ),
  'last_name' => array(
    '#title' => t('Last name'),
    '@L[fr]#title' => 'Nom de famille',
    '#required' => TRUE,
    '@extends' => 'name',
  ),  
  'organization' => array(
    '#title' => t('Organization'),
    '@L[fr]#title' => 'Organisme',
    '#type' => 'textfield',
  ),
  'email' => array(
    '#required' => TRUE,
    '@extends' => 'email',
  ),
);

templates::$field_templates['up_to_date_contact_info'] = array(  
  '#type' => 'fieldset',
  
  'up_to_date_question_fieldset' => array(
    '#type' => 'fieldset',
    '#title' => t('Does the Agency have up-to-date contact information for you?'),//<span class="form-required badge" title="This field is required.">Required</span>'),
    '@L[fr]#title' => "L'Office a-t-il vos coordonnées les plus récentes?",//<span class=\"form-required badge\" title=\"Ce champ est requis.\">Obligatoire</span>",
    
    'up_to_date_question' => array(
      '#required' => TRUE,
      '#type' => 'radios',
      '#options' => array(
        'Y' => 'Yes',
        'N' => 'No',
      ),
      '@L[fr]#options' => array(
        'Y' => 'Oui',
        'N' => 'Non',
      ),
      '@ajax_send' => TRUE,
    ),
  ),
  
  'up_to_date_contact_fieldset' => array(
    '#type' => 'fieldset',
    '#title' => t('New contact information'),
    '@L[fr]#title' => "Nouvelles coordonnées",
    '@inheritable_prefix' => 'new_',
    '@dependencies' => array(
      'value1' => '{up_to_date_question}',
      'operator' => '==',
      'value2' => 'N',
    ),
    
    'organization' => array(
      '#title' => t('Organization'),
      '@L[fr]#title' => 'Organisme',
      '#type' => 'textfield',
    ),    
    'street_address' => array(
      '#title' => t('Street address'),
      '@L[fr]#title' => 'Adresse (rue)',
      '#type' => 'textfield',
    ),
    'city' => array(
      '#title' => t('City'),
      '@L[fr]#title' => 'Ville',
      '#type' => 'textfield',
    ),
    'province' => array(
      '@extends' => 'province',
      '@required_label' => 'Required for Canada/US',
      '@L[fr]@required_label' => "Obligatoire pour le Canada/les États-Unis",
    ),
    'postal_code' => array(
      '@extends' => 'postal_code',
      '@required_label' => 'Required for Canada/US',
      '@L[fr]@required_label' => "Obligatoire pour le Canada/les États-Unis",
    ),
    'country' => array(
      '@extends' => 'country',
    ),
    'email' => array(
      '@extends' => 'email',
    ),
    'fax' => array(
      '@extends' => 'fax',
    ),
    'phone_and_extension' => array(
      '#title' => t('Primary telephone'),
      '@L[fr]#title' => 'Numéro de téléphone principal',
      '@extends' => 'phone_and_extension',
    ),
  ),
);


templates::$field_templates['contact_info'] = array(  
  '#type' => 'fieldset',
  
  'first_name' => array(
    '#title' => 'First name',
    '@L[fr]#title' => 'Prénom',
    '@extends' => 'name',
  ),
  'last_name' => array(
    '#title' => t('Last name'),
    '@L[fr]#title' => 'Nom de famille',
    '@extends' => 'name',
  ),  
  'organization' => array(
    '#title' => t('Organization'),
    '@L[fr]#title' => 'Organisme',
    '#type' => 'textfield',
  ),
  'relationship_org' => array(
    '#title' => t('Relationship to the organization'),
    '@L[fr]#title' => "Lien avec l'organisme",
    '#type' => 'textfield',
    '@dependencies' => FALSE,
  ),
  'same_as' => array(
    '@extends' => 'same_as',
    '@dependencies' => FALSE,
  ),
  'street_address' => array(
    '#title' => t('Street address'),
    '@L[fr]#title' => 'Adresse (rue)',
    '#type' => 'textfield',
  ),
  'city' => array(
    '#title' => t('City'),
    '@L[fr]#title' => 'Ville',
    '#type' => 'textfield',
  ),
  'province' => array(
    '@extends' => 'province',
    '@required_label' => 'Required for Canada/US',
    '@L[fr]@required_label' => "Obligatoire pour le Canada/les États-Unis",
  ),
  'postal_code' => array(
    '@extends' => 'postal_code',
    '@required_label' => 'Required for Canada/US',
    '@L[fr]@required_label' => "Obligatoire pour le Canada/les États-Unis",
  ),
  'country' => array(
    '@extends' => 'country',
  ),
  'email' => array(
    '@extends' => 'email',
  ),
  'fax' => array(
    '@extends' => 'fax',
  ),
  'phone_and_extension' => array(
    '#title' => t('Primary telephone'),
    '@L[fr]#title' => 'Numéro de téléphone principal',
    '@extends' => 'phone_and_extension',
  ),
);

templates::$field_templates['contact_info_required'] = array(  
  '#type' => 'fieldset',
  '@extends' => 'contact_info',
  
  'first_name' => array(
    '#required' => TRUE,
  ),
  'last_name' => array(
    '#required' => TRUE,
  ),  
  
  'street_address' => array(
    '#required' => TRUE,
  ),
  'city' => array(
    '#required' => TRUE,
  ),
  'province' => array(
    '#required' => TRUE,
  ),
  'postal_code' => array(
    '#required' => TRUE,
  ),
  'country' => array(
    '#required' => TRUE,
  ),
  'email' => array(
    '#required' => TRUE,
  ),
  'phone_and_extension' => array(
    'phone_number' => array(
      '#required' => TRUE,
    )
  ),
);



templates::$field_templates['applicant_contact_group'] = array(
  '#type' => 'group',
  '@inheritable_prefix' => 'applicant_',
  '@init_variables' => array(
    // Both these are global
    '<applicant_contact_copies>' => 1,
  ),
  'contact_info' => array(
    '@extends' => 'contact_info_required',
    '#type' => 'fieldset',
    '@#title' => array(
      'condition' => array(
        'value1' => '<B:applicant_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Contact Information: Applicant'),
      'source_false' => t('Contact Information: Applicant') . '[B: applicant_contact_counter]',
    ),
    '@L[fr]@#title' => array(
      'condition' => array(
        'value1' => '<B:applicant_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => 'Coordonnées : demandeur',
      'source_false' => 'Coordonnées : demandeur' . '[B: applicant_contact_counter]',
    ),
    'organization' => array(
      '@dependencies' => FALSE,
    ),
    //'@inheritable_suffix' => '<B:fieldset_copy>',
    '@build_loop' => array(
      'start' => 1,
      'end' => '<B:applicant_contact_copies>',
      'iterator_variable' => '[applicant_contact_counter]',
      'max_loops' => 10,
    ),
  ),
  'remove_button' => array(
    '@extends' => 'remove_button',
    '#value' => t('-Remove applicant'),
    '@L[fr]#value' => 'Supprimer un demandeur',
    '#name' => 'remove_app',
    '@dependencies' => array(
      'value1' => '<applicant_contact_copies>',
      'operator' => '>=',
      'value2' => '2',
    ),
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("applicant_contact_copies", DF\get_global_variable("applicant_contact_copies") - 1);',
      ),
    ),
  ),
  'add_button' => array(
    '@extends' => 'add_button',
    '#value' => t('+Add another applicant'),
    '@L[fr]#value' => '+Ajouter un autre demandeur',
    '#name' => 'add_app',
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("applicant_contact_copies", DF\get_global_variable("applicant_contact_copies") + 1);',
      ),
    ),
  ),
);


templates::$field_templates['organization_contact_group'] = array(
  '#type' => 'group',
  '@inheritable_prefix' => 'organization_',  
  '@init_variables' => array(
    '<organization_contact_copies>' => 1,
  ),
  
  'contact_info' => array(
    '@extends' => 'contact_info_required',
    '#type' => 'fieldset',
    '@#title' => array(
      'condition' => array(
        'value1' => '<B:organization_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Contact information: Contact person for the organization'),
      'source_false' => t('Contact information: Contact person for the organization') . '[B: organization_contact_counter]',
    ),
    '@L[fr]@#title' => array(
      'condition' => array(
        'value1' => '<B:organization_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => "Coordonnées : personne-ressource de l'organisme",
      'source_false' => "Coordonnées : personne-ressource de l'organisme" . '[B: organization_contact_counter]',
    ),
    'organization' => array(
      '#required' => TRUE,
    ),
    'relationship_org' => array(
      '@dependencies' => TRUE,
    ),
    'street_address' => array(
      '#title' => 'Street address of the organization',
      '@L[fr]#title' => "Adresse de l'organisme",
    ),
    //'@inheritable_suffix' => '<B:fieldset_copy>',
    '@build_loop' => array(
      'start' => 1,
      'end' => '<B:organization_contact_copies>',
      'iterator_variable' => '[organization_contact_counter]',
      'max_loops' => 10,
    ),
  ),
  'remove_button' => array(
    '@extends' => 'remove_button',
    '#value' => t('-Remove organization'),
    '@L[fr]#value' => 'Supprimer un organisme',
    '#name' => 'remove_org',
    '@dependencies' => array(
      'value1' => '<organization_contact_copies>',
      'operator' => '>=',
      'value2' => '2',
    ),
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("organization_contact_copies", DF\get_global_variable("organization_contact_copies") - 1);',
      ),
    ),
  ),
  'add_button' => array(
    '@extends' => 'add_button',
    '#value' => t('+Add another organization'),
    '@L[fr]#value' => 'Ajouter un autre organisme',
    '#name' => 'add_org',
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("organization_contact_copies", DF\get_global_variable("organization_contact_copies") + 1);',
      ),
    ),
  ),
);

templates::$field_templates['lawyer_contact_group'] = array(
  '#type' => 'group',
  '@inheritable_prefix' => 'lawyer_',
  
  'contact_info' => array(
    '@extends' => 'contact_info_required',
    '#type' => 'fieldset',    
    '#title' => t('Contact information: Lawyer'),
    '@L[fr]#title' => 'Coordonnées : avocat',
    
    'organization' => array(
      '#title' => t('Organization/firm'),
      '@L[fr]#title' => 'Organisme/entreprise',
      '#required' => TRUE,
    ),
  ),  
);


templates::$field_templates['representative_contact_group'] = array(
  '#type' => 'group',
  '@inheritable_prefix' => 'representative_',
  
  'contact_info' => array(
    '@extends' => 'contact_info_required',
    '#type' => 'fieldset',
    
    '#title' => t('Contact Information: Representative'),
    '@L[fr]#title' => 'Coordonnées : représentant',
    
    'organization' => array(
      '@dependencies' => FALSE,
    ),
  ),
);


templates::$field_templates['respondent_contact_group'] = array(
  '#type' => 'group',
  '@inheritable_prefix' => 'respondent_',  
  '@init_variables' => array(
    '<respondent_contact_copies>' => 1,
  ),
  
  'contact_info' => array(
    '@extends' => 'contact_info',
    '#type' => 'fieldset',
    
    '@#title' => array(
      'condition' => array(
        'value1' => '<B:respondent_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Contact Information: Respondent'),
      'source_false' => t('Contact Information: Respondent') . '[B: respondent_contact_counter]',
    ),
    '@L[fr]@#title' => array(
      'condition' => array(
        'value1' => '<B:respondent_contact_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Coordonnées : défendeur'),
      'source_false' => t('Coordonnées : défendeur') . '[B: respondent_contact_counter]',
    ),
    
    'info_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => 'The contact information for the respondent should be for the last person you interacted with for the purposes of trying to resolve your dispute. Please provide as much information as possible.',
      '@L[fr]#markup' => 'Les coordonnées du défendeur doivent être celles de la dernière personne avec qui vous avez communiqué pour tenter de régler le différend. Veuillez fournir autant de renseignements que possible.',
      '#weight' => -10,
      '@dependencies' => FALSE,
    ),
    
    'organization' => array(
      '#weight' => -5,
    ),
    
    //'@inheritable_suffix' => '<B:fieldset_copy>',
    '@build_loop' => array(
      'start' => 1,
      'end' => '<B:respondent_contact_copies>',
      'iterator_variable' => '[respondent_contact_counter]',
      'max_loops' => 10,
    ),
  ),
  'remove_button' => array(
    '@extends' => 'remove_button',
    '#value' => t('-Remove respondent'),
    '@L[fr]#value' => 'Supprimer un défendeur',
    '#name' => 'remove_respondent',
    '@dependencies' => array(
      'value1' => '<respondent_contact_copies>',
      'operator' => '>=',
      'value2' => '2',
    ),
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("respondent_contact_copies", DF\get_global_variable("respondent_contact_copies") - 1);',
      ),
    ),
  ),
  'add_button' => array(
    '@extends' => 'add_button',
    '#value' => t('+Add another respondent'),
    '@L[fr]#value' => 'Ajouter un autre défendeur',
    '#name' => 'add_respondent',
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("respondent_contact_copies", DF\get_global_variable("respondent_contact_copies") + 1);',
      ),
    ),
  ),
);


templates::$field_templates['supporting_docs_options'] = array(
  'have_non_conf' => 'I have supporting documents.',
  'have_conf' => "I have confidential supporting documents and I'm filing a request for confidentiality",
);

templates::$field_templates['supporting_docs_options_fr'] = array(
  'have_non_conf' => "J'ai des documents à l'appui.",
  'have_conf' => "J'ai des documents confidentiels  à l'appui et je dépose une requête de confidentialité.",
);

templates::$field_templates['have_supporting_docs'] = array(
  '#type' => 'fieldset',
  '#title' => 'Do you have supporting documents?',
  '@L[fr]#title' => "Avez-vous des documents à l'appui?",
  
  'have_supporting_docs_checkboxes' => array(
    '#type' => 'checkboxes',
    '@ajax_send' => TRUE,
    '#options' => templates::$field_templates['supporting_docs_options'],
    '@L[fr]#options' => templates::$field_templates['supporting_docs_options_fr'],
  ),
  
  'have_supporting_docs_description' => array(
    '@extends' => 'description_text',
    '#markup' => '',
    '@L[fr]#markup' => "",
  ),
);

templates::$field_templates['list_supporting_docs'] = array(
  '#type' => 'fieldset',
  '#title' => 'Supporting documents',
  '@L[fr]#title' => "Documents à l'appui",  
  '@dependencies' => array(
    'value1' => 'have_non_conf',
    'operator' => 'is_in',
    'value2' => '{have_supporting_docs_checkboxes}',
  ),
  
  'list_supporting_docs_textarea' => array(
    '#type' => 'textarea',
    '@#title' => 'List the documents you are relying on to support your <form_short_name>.',
    '@L[fr]@#title' => "Dressez la liste des documents à l'appui de votre <form_short_name_fr>.",
  ),
  
  'list_supporting_docs_question' => array(
    '@extends' => 'question_text2',
    'heading' => array(
      '#markup' => 'How do I file my documents?',
      '@L[fr]#markup' => "Comment dois-je déposer mes documents? ",
    ),
    'body' => array(
      '#markup' => 'After you submit the form, you will be emailed a link to a secure file transfer system. You will have an account to manage your documents.</p>
                    <p>Please upload your files right away.</p>
                    <p>You can also file documents by fax, courier, or personal delivery.',
      '@L[fr]#markup' =>    "Une fois que vous aurez soumis le formulaire, vous recevrez par courriel un lien vers un système sécurisé de transfer de fichiers. Vous aurez un compte qui vous permettra de gérer vos documents.</p>"
                          . "<p>Veuillez télécharger vos fichiers immédiatement.</p>"
                          . "<p>Vous avez aussi la possibilité de déposer des documents par télécopieur, par messagerie, ou en main propre.",
    ),    
  ),
  
);




templates::$field_templates['confidential_docs'] = array(
  '#type' => 'fieldset',
  '#title' => t('Confidential documents'),
  '@L[fr]#title' => "Documents confidentiels",
  '@dependencies' => array(
    'value1' => 'have_conf',
    'operator' => 'is_in',
    'value2' => '{have_supporting_docs_checkboxes}',
  ),
  
  'confidential_docs_info' => array(
    '@extends' => 'info_text2',
    'heading' => array(
      '#markup' => 'Documents required as part of a request for confidentiality',
      '@L[fr]#markup' => "Documents requis dans le cadre d'une requête de confidentialité",
    ),
    'body' => array(
      '#markup' => 'One public version of the document from which the confidential information has been redacted, which is sent to all parties; and</p>
                    <p>One confidential version of the document, which:</p>
                    <ul>
                      <li>contains and identifies the confidential information that was redacted from the public version; and</li>
                      <li>states "CONTAINS CONFIDENTIAL INFORMATION" on the top of each page.</li>
                    </ul>
                    <p>The confidential version is only filed with the Agency at this stage in the proceedings.',
      '@L[fr]#markup' => "Une version publique du document duquel les renseignements confidentiels ont été retirés, laquelle version est transmise à toutes les parties;</p>
                        <p>Une version confidentielle du document qui :</p>
                        <ul>
                          <li>contient les renseignements confidentiels et qui indique les passages qui ont été retirés de la version publique;</li>
                          <li>porte la mention « contient des renseignements confidentiels » au haut de chaque page.</li>
                        </ul>
                        <p>La version confidentielle du document est déposée auprès de l'Office seulement à ce stade de l'instance.",
    ),    
  ),
  
  'identify_confidential' => array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => 'Identify the document or portion of the document that contains confidential information.',
    '@L[fr]#title' => "Indiquez quel document ou quelle partie du document contient des renseignements confidentiels.",
  ),
  
  'list_parties' => array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => 'List the parties, if any, with whom you would be willing to share the document.',
    '@L[fr]#title' => "Dressez, le cas échéant, la liste des parties qui seraient autorisées à avoir accès au document.",
  ),
  
  'reasons_in_support' => array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => 'Set out in detail the reasons in support of your request for confidentiality, including an explanation of the relevance to the dispute proceeding and any specific direct harm that might result from its disclosure.',
    '@L[fr]#title' => "Détaillez les motifs à l'appui de votre requête de confidentialité, notamment une explication de la pertinence du document à l'égard de l'instance et la description du préjudice direct précis qui pourrait résulter de la divulgation des renseignements confidentiels.",
  ),
  
  'list_docs_filing' => array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => 'List the documents that you will be filing.',
    '@L[fr]#title' => "Dressez la liste des documents que vous déposerez.",
  ),
  
    
  'conf_how_to_file' => array(
    '@extends' => 'question_text2',
    'heading' => array(
      '#markup' => 'How do I file my documents?',
      '@L[fr]#markup' => "Comment dois-je déposer mes documents?",
    ),
    'body' => array(
      '#markup' => 'After you submit the form, you will be emailed a link to a secure file transfer system. You will have an account to manage your documents.</p>
                    <p>Please upload your files right away.</p>
                    <p>You can also file documents by fax, courier, or personal delivery.',
      '@L[fr]#markup' => "Une fois que vous aurez soumis le formulaire, vous recevrez par courriel un lien vers un système sécurisé de transfert de fichiers. Vous aurez un compte qui vous permettra de gérer vos documents.</p>
                        <p>Veuillez télécharger vos fichiers immédiatement.</p>
                        <p>Vous avez aussi la possibilité de déposer les documents par télécopieur, par messagerie, ou en main propre.",
    ),
  ),
  
  'what_happens_next' => array(
    '@extends' => 'description_text',    
    '#markup' => '<h2>What happens next?</h2><p>A person may oppose your request for confidentiality within 5 business days after the day on which they receive it by filing a request for disclosure. You may respond to any request for disclosure in accordance with subsection 31(4) (Form 14) within 3 business days after the day on which you receive the request for disclosure.</p>
                    <p>The Agency will consider whether to grant the request after reviewing all the submissions of the parties.</p>
                    <p>If the Agency denies your request for confidentiality, your document will be placed on the public record.</p>
                    <p>If your request is granted, the Agency may order that a part or the whole document be placed on the confidential record or be provided to the other parties on the basis that it be kept strictly confidential.</p>
                    <p>Refer to section 31 of the Annotation for more information.',
     '@L[fr]#markup' => "<h2>Quelles sont les prochaines étapes?</h2><p>Une personne peut s'opposer à votre requête de confidentialité en déposant une requête de divulgation dans les cinq jours ouvrables suivant la date de réception de la copie de la requête.  Vous avez la possibilité de déposer auprès de l'Office une réponse écrite à une requête de divulgation dans les trois jours ouvrables suivant la date de réception de la copie de la requête de divulgation, conformément au paragraphe 31(4) (formulaire 14).</p>
                    <p>L'Office examinera toutes les présentations des parties, après quoi il décidera d'acquiescer ou non à la requête.</p>
                    <p>Si l'Office rejette votre requête de confidentialité, votre document sera versé aux archives publiques.</p>
                    <p>Si l'Office acquiesce à votre requête, il pourrait ordonner qu'une version ou une partie du document soit versée aux archives publiques ou qu'il soit fourni aux autres parties de façon strictement confidentielle.</p>
                    <p>Pour plus de renseignements, consultez l'article 31 de l'Annotation.</p>
                    <p>Pour plus de renseignements, consultez l'article 31 des Règles pour le règlement des différends.",
    
    )
);


templates::$field_templates['personal_info'] = array(
  '#type' => 'fieldset',
  '#title' => t('Personal Information Collection Statement'),
  '@L[fr]#title' => "Énoncé de collecte de renseignements personnels",
  '#attributes' => array(
    'id' => array('edit-personal-info'),
  ),
    
  'personal_info_text_markup' => array(
    '#markup' =>  '</>Please read the '
                  . '<a id="pics-link" href="/sites/default/files/tco-for-social-publishing.pdf" target="_blank">'
                  . 'Personal Information Collection Statement' 
                  . '</a>' 
                  . ' and click I have read" below (opens in a new window).</p>',
    '@L[fr]#markup' => "<p>Veuillez lire "
                  . '<a id="pics-link" href="/sites/default/files/tco-for-social-publishing.pdf" target="_blank">'
                  . "l'Énoncé de collecte de renseignements personnels"
                  . '</a>'
                  . ", puis cliquer sur la touche « J'ai lu » ci-dessous (une nouvelle fenêtre s'ouvrira).</p>",
  ),
  'personal_info_acknowledgement' => array(
    '#type' => 'checkbox',
    '#required' => TRUE,
    '#attributes' => array(
      'id' => array("accept-personal-statement"),
    ),
    //'@required_label' => '',
    '#title' => t('I have read and understood the Personal Information Collection Statement'),
    '@L[fr]#title' => "J'ai lu et compris le contenu de l'Énoncé de collecte de renseignements personnels.",
    //'#disabled' => TRUE,
  ),
);


templates::$field_templates['case_id'] = array(  
  '#type' => 'fieldset',
  '#title' => 'Case identification',
  '@L[fr]#title' => 'Identification du dossier',
  
  'applicant_names' => array(
    '#title' => 'Name of applicant(s)',
    '@L[fr]#title' => 'Nom du ou des demandeurs',
    '#required' => TRUE,
    '@extends' => 'name',
  ),
  'respondent_names' => array(
    '#title' => t('Name of respondent(s)'),
    '@L[fr]#title' => 'Nom du ou des défendeurs',
    '#required' => TRUE,
    '@extends' => 'name',
  ),
  'case_num' => array(
    '#title' => t('Case number'),
    '@L[fr]#title' => 'Numéro du dossier',
    '#required' => TRUE,
    '#type' => 'textfield',
  ),  
);


templates::$field_templates['email_party_contact'] = array(
  '#type' => 'fieldset',
  
  'party_name' => array(
    '#title' => t('Name of party'),
    '@L[fr]#title' => 'Nom de la partie',
    '#required' => TRUE,
    '@extends' => 'name',
  ),
  
  'party_email' => array(
    '#title' => t('Email address'),
    '@L[fr]#title' => 'Adresse électronique',
    '#required' => TRUE,
    '@extends' => 'email',
  ),
);

templates::$field_templates['email_party_group'] = array(
  '#type' => 'group',  
  '@inheritable_prefix' => 'email_',  
  '@init_variables' => array(
    '<email_party_copies>' => 1,
  ),
  
  'contact_info' => array(
    '@extends' => 'email_party_contact',
    
    '@#title' => array(
      'condition' => array(
        'value1' => '<B:email_party_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Party'),
      'source_false' => t('Party') . '[B: email_party_counter]',
    ),
    '@L[fr]@#title' => array(
      'condition' => array(
        'value1' => '<B:email_party_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Partie'),
      'source_false' => t('Partie') . '[B: email_party_counter]',
    ),
        
    //'@inheritable_suffix' => '<B:fieldset_copy>',
    '@build_loop' => array(
      'start' => 1,
      'end' => '<B:email_party_copies>',
      'iterator_variable' => '[email_party_counter]',
      'max_loops' => 10,
    ),
  ),
  'remove_button' => array(
    '@extends' => 'remove_button',
    '#value' => t('-Remove party'),
    '#name' => 'email_remove',
    '@L[fr]#value' => 'Supprimer une partie',
    '@dependencies' => array(
      'value1' => '<email_party_copies>',
      'operator' => '>=',
      'value2' => '2',
    ),
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("email_party_copies", DF\get_global_variable("email_party_copies") - 1);',
      ),
    ),
  ),
  'add_button' => array(
    '@extends' => 'add_button',
    '#value' => t('+Add another party'),
    '#name' => 'email_add',
    '@L[fr]#value' => 'Ajouter une autre partie',
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("email_party_copies", DF\get_global_variable("email_party_copies") + 1);',
      ),
    ),
  ),
);

templates::$field_templates['submission_to_parties_email'] = array(
  '#type' => 'fieldset',
  '#title' => t('Submission to parties by email'),
  '@L[fr]#title' => 'Présentation aux parties par courriel',
  
  'do_does_this_work' => array(
    '@extends' => 'question_text2',
    
    'heading' => array(
      '#markup' => 'How does this work?',
      '@L[fr]#markup' => "Quel est le processus?",
    ),
    'body' => array(
      '#markup' => 'A PDF copy of this form will be emailed to the parties upon submission.</p>'
                 . '<p>If you are filing supporting documents, they will automatically be sent to any parties listed below after you upload them to the secure file transfer system.',
      '@L[fr]#markup' => "Une copie PDF de ce formulaire sera transmise par courriel aux parties, au moment de la transmission du formulaire. Si vous déposez des documents à l'appui, ils seront automatiquement transmis à toute partie figurant sur la liste ci-dessous après que vous aurez chargé leur nom au système sécurisé de transfert de fichiers.",
    ),
  ),
  
  'email_party_fieldset' => array(
    '#type' => 'fieldset',
    
    'email_party_group' => array(
      '@extends' => 'email_party_group'
    )
  )

);

templates::$field_templates['fax_party_contact'] = array(
  '#type' => 'fieldset',
  
  'party_name' => array(
    '#title' => t('Name of party'),
    '@L[fr]#title' => 'Nom de la partie',
    '#required' => TRUE,    
    '@extends' => 'name',
  ),
  
  'party_address_fax' => array(
    '#title' => t('Address or fax'),
    '@L[fr]#title' => 'Adresse ou télécopieur',
    '#required' => TRUE,
    '#type' => 'textfield',
  ),
);

templates::$field_templates['fax_party_group'] = array(
  '#type' => 'group',  
  '@inheritable_prefix' => 'fax_',  
  '@init_variables' => array(
    '<fax_party_copies>' => 1,
  ),
  
  'contact_info' => array(
    '@extends' => 'fax_party_contact',
    
    '@#title' => array(
      'condition' => array(
        'value1' => '<B:fax_party_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Party'),
      'source_false' => t('Party') . '[B: fax_party_counter]',
    ),
    '@L[fr]@#title' => array(
      'condition' => array(
        'value1' => '<B:fax_party_copies>',
        'operator' => '==',
        'value2' => 1,
        'eval_time' => 'B',
      ),
      'source_true' => t('Partie'),
      'source_false' => t('Partie') . '[B: fax_party_counter]',
    ),
        
    //'@inheritable_suffix' => '<B:fieldset_copy>',
    '@build_loop' => array(
      'start' => 1,
      'end' => '<B:fax_party_copies>',
      'iterator_variable' => '[fax_party_counter]',
      'max_loops' => 10,
    ),
  ),
  'remove_button' => array(
    '@extends' => 'remove_button',
    '#value' => t('-Remove party'),
    '@L[fr]#value' => 'Supprimer une partie',
    '#name' => 'fax_remove',
    '@dependencies' => array(
      'value1' => '<fax_party_copies>',
      'operator' => '>=',
      'value2' => '2',
    ),
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("fax_party_copies", DF\get_global_variable("fax_party_copies") - 1);',
      ),
    ),
  ),
  'add_button' => array(
    '@extends' => 'add_button',
    '#value' => t('+Add another party'),
    '#name' => 'fax_add',
    '@L[fr]#value' => 'Ajouter une autre partie',
    '#ajax' => array(
      'build_action' => 'dynamic_forms_execute',
      'build_action_args' => array(
        'statement' => 'DF\set_global_variable("fax_party_copies", DF\get_global_variable("fax_party_copies") + 1);',
      ),
    ),
  ),
);

templates::$field_templates['submission_to_parties_fax'] = array(
  '#type' => 'fieldset',
  '#title' => t('Submission to parties by fax, courier or personal delivery'),
  '@L[fr]#title' => 'Transmission aux parties par télécopieur, par messagerie ou en main propre.',
  
  'do_does_this_work' => array(
    '@extends' => 'question_text2',
    
    'heading' => array(
      '#markup' => 'How does this work?',
      '@L[fr]#markup' => "Quel est le processus?",
    ),
    'body' => array(
      '#markup' => 'Upon submission, you will receive a PDF copy of your form.</p>'
                 . '<p>The Agency and all other parties must receive your form and any supporting documents on the same day. You are responsible for meeting the deadlines for filing.'
                 . '<p>For more information, refer to section 8 of the Dispute Adjudication Rules.',
      '@L[fr]#markup' => "Une copie PDF de votre formulaire sera transmise par courriel aux parties au moment de la transmission de votre formulaire.</p>"
                        ."<p>L'Office et toutes les autres parties doivent recevoir votre formulaire et les documents à l'appui, le même jour. Vous êtes responsable du respect des délais relatifs au dépôt.</p>"
                        ."<p>Pour plus de renseignements, consultez l'article 8 des Règles pour le règlement des différends.",
    ), 
  ),
  
  'fax_party_fieldset' => array(
    '#type' => 'fieldset',
    
    'fax_party_group' => array(
      '@extends' => 'fax_party_group'
    )
  )

);


templates::$field_templates['submission_sending_options'] = array(
  'email' => 'Automatically (by email as I submit this form).',
  'fax' => "Myself (by fax, courier, or personal delivery after I submit the form).",
);

templates::$field_templates['submission_sending_options_fr'] = array(
  'email' => "Automatiquement (par courriel, en même temps que je dépose ce formulaire).",
  'fax' => "Moi-même (par courriel, par messagerie, ou en main propre après avoir déposé le formulaire).",
);

// Define the submission page
templates::$field_templates['submission_page'] = array(
  
  'page_heading' => array(
    '@extends' => 'page_heading',
    '@#markup' => t('Part 3 of 3: Submission'),
    '@L[fr]@#markup' => 'Partie 3 de 3 : Présentation',
  ),
  
  'filing_requirements' => array(
    '@extends' => 'info_text2',
    'heading' => array(
      '#markup' => 'Filing requirements',
      '@L[fr]#markup' => "Exigences relatives au dépôt ",
    ),
    'body' => array(
      '#markup' => 'Your form and any supporting documents must be sent to the Agency and all other parties on the same day.',
      '@L[fr]#markup' => "Vous devez transmettre votre formulaire et les documents à l'appui à l'Office et à toutes les autres parties le même jour.",
    ),
  ),
  
  'sending_supporting_documents' => array(
    '#type' => 'fieldset',
    '#title' => 'How do you want to send this form and any supporting documents to the parties?',//<span class="form-required badge" title="This field is required.">Required</span>',
    '@L[fr]#title' => "De quelle façon voulez-vous transmettre ce formulaire et tous les documents à l'appui aux parties?",//<span class=\"form-required badge\" title=\"Ce champ est requis.\">Obligatoire</span>",

    'sending_supporting_documents_checkboxes' => array(
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '@ajax_send' => TRUE,
      '#options' => templates::$field_templates['submission_sending_options'],
      '@L[fr]#options' => templates::$field_templates['submission_sending_options_fr'],
    ),
  ),
     
  'submission_to_parties_email' => array(
    '@extends' => 'submission_to_parties_email',
    '@dependencies' => array(
      'value1' => 'email',
      'operator' => 'is_in',
      'value2' => '{sending_supporting_documents_checkboxes}',
    ),
  ),
  
  'submission_to_parties_fax' => array(
    '@extends' => 'submission_to_parties_fax',
    '@dependencies' => array(
      'value1' => 'fax',
      'operator' => 'is_in',
      'value2' => '{sending_supporting_documents_checkboxes}',
    ),
  ),
);


templates::$field_templates['post_submission'] = array(  
  '#markup' => 'We have successfully received your submission.',
  '@L[fr]#markup' => 'We have successfully received your submission.',  
);

templates::$field_templates['post_submission_5_10'] = array(
  '#markup' => 'We have successfully received your submission.',
  '@L[fr]#markup' => 'We have successfully received your submission.',
);
