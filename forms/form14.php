<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>A person or party who wishes to respond to a general or specific request.</p>
<table class="table-simplify" id="table-1001">
<thead>
<tr><th id="table-1001-r1-c1" scope="col" width="198">Type of request to which you are responding</th><th id="table-1001-r1-c2" scope="col" width="125">Reference</th><th id="table-1001-r1-c3" scope="col" width="138">Who can file a response</th><th id="table-1001-r1-c4" scope="col" width="175">Time limit for filing response</th></tr>
</thead>
<tbody>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>General request</p>
</td>
<td width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-1" target="_blank">subsection 27(2)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>A party</p>
</td>
<td headers="table-1001-r1-c4" width="175">
<p>Within 5 business days after the day on which you receive a copy of the request.</p>
</td>
</tr>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>Request for expedited process</p>
</td>
<td headers="table-1001-r1-c2" width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-2" target="_blank">subsection 28(4)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>A party</p>
</td>
<td headers="table-1001-r1-c4" width="175">
<p>Within 1 business day after the day on which you receive a copy of the request.</p>
</td>
</tr>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>Request to extend or shorten time limit</p>
</td>
<td headers="table-1001-r1-c2" width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-4" target="_blank">subsection 30(2)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>A party</p>
</td>
<td headers="table-1001-r1-c4" rowspan="4" valign="top" width="175">
<p>Within 3 business days after the day on which you receive a copy of the request.</p>
</td>
</tr>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>Request for disclosure</p>
</td>
<td headers="table-1001-r1-c2" width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-5" target="_blank">subsection 31(4)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>Any person or party</p>
</td>
</tr>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>Request to amend a document</p>
</td>
<td headers="table-1001-r1-c2" width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-7" target="_blank">paragraph&nbsp; 33(2)(a)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>A party</p>
</td>
</tr>
<tr>
<td headers="table-1001-r1-c1" width="198">
<p>Request to file document whose filing is not otherwise provided for in Rules</p>
</td>
<td headers="table-1001-r1-c2" width="125">
<p><a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-8" target="_blank">paragraph 34(2)(a)</a></p>
</td>
<td headers="table-1001-r1-c3" width="138">
<p>A party</p>
</td>
</tr>
</tbody>
</table>
<h2>Purpose</h2>
<p>To respond to a request made by another person or party.</p>
<p>Refer to subsections 27(2), 28(4), 30(2), 31(4), 33(2) and 34(2) of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>The party who submitted the request will have the opportunity to reply to your response. The Agency will then decide whether to approve or deny the request.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our&nbsp;<a href="http://23.23.154.142/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  
  
  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Une personne ou une partie qui souhaite répondre à une requête générale ou spécifique.</p>
<table class=\"table-simplify\" id=\"table-1001\">
<thead>
<tr><th id=\"table-1001-r1-c1\" scope=\"col\" width=\"198\">Type de requête à laquelle vous répondez&nbsp;</th><th id=\"table-1001-r1-c2\" scope=\"col\" width=\"125\">Référence</th><th id=\"table-1001-r1-c3\" scope=\"col\" width=\"138\">Qui peut déposer une réponse</th><th id=\"table-1001-r1-c4\" scope=\"col\" width=\"175\">Délais prévus pour déposer une réponse</th></tr>
</thead>
<tbody>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Requête générale</p>
</td>
<td width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-1\" target=\"_blank\">paragraphe 27(2)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c4\" width=\"175\">
<p>Dans les cinq jours ouvrables suivant la date de réception de la copie de la requête</p>
</td>
</tr>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Requête de procédure accélérée</p>
</td>
<td headers=\"table-1001-r1-c2\" width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-2\" target=\"_blank\"><span>paragraphe</span>&nbsp;28(4)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c4\" width=\"175\">
<p>Au plus tard un jour ouvrable après la date de réception de la copie de la requête&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Requête de prolongation ou d’abrégement de délais&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c2\" width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-4\" target=\"_blank\"><span>paragraphe</span>&nbsp;30(2)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Une partie&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c4\" rowspan=\"4\" valign=\"top\" width=\"175\">
<p>Dans les trois jours ouvrables suivant la date de réception de la copie de la requête<strong></strong></p>
</td>
</tr>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Request for disclosure</p>
</td>
<td headers=\"table-1001-r1-c2\" width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-5\" target=\"_blank\"><span>paragraphe&nbsp;</span>31(4)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Toute personne ou partie</p>
</td>
</tr>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Requête de modification de document&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c2\" width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-7\" target=\"_blank\">alinéa 33(2)<em>a</em>)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Une partie&nbsp;</p>
</td>
</tr>
<tr>
<td headers=\"table-1001-r1-c1\" width=\"198\">
<p>Requête de dépôt de document dont le dépôt n'est pas prévu par les règles&nbsp;</p>
</td>
<td headers=\"table-1001-r1-c2\" width=\"125\">
<p><a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-8\" target=\"_blank\">alinéa 34(2)<em>a</em>)</a></p>
</td>
<td headers=\"table-1001-r1-c3\" width=\"138\">
<p>Une partie&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<h2>But</h2>
<p>Répondre à une requête présentée par une autre personne ou partie.</p>
<p>Veuillez consulter les paragraphes 27(2), 28(4), 30(2), 31(4), 33(2) et 34(2) des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>La partie qui a déposé la requête aura la possibilité de répliquer à votre réponse. L’Office décidera ensuite d’accorder ou de rejeter la requête.&nbsp;</p>
<h2>Collecte de renseignements personnels</h2>
<p>Veuillez consulter notre&nbsp;<a href=\"http://23.23.154.142/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a>&nbsp;pour de plus amples renseignements.</p>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'response to request',
      '<form_short_name_fr>' => 'réponse à une requête',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs au <form_short_name_fr>',
    ),
    
    'specify_request' => array(
      '#type' => 'textarea',
      '#title' => t('Specify which request you are responding to, including the name of the person who filed the request'),
      '@L[fr]#title' => 'Précisez à quelle requête vous répondez, y compris le nom de la personne qui a déposé la requête.',
      '#required' => TRUE,
    ),
    
    
    'fieldset_responding' => array(
      '#type' => 'fieldset',
      '#title' => t('Do you agree with the response?'),
      '@L[fr]#title' => "Êtes-vous d'accord avec la réponse?",
      
      'responding' => array(
        '#required' => TRUE,
        '#type' => 'radios',
        '@ajax_send' => TRUE,
        //'@required_label' => '',
        '#options' => array(
          'Y' => t('Yes'),
          'N' => t('No'),
        ),
        '@L[fr]#options' => array(
          'Y' => 'Oui',
          'N' => 'No',
        ),
      ),
    ),
    
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Set out the arguments in support of your position'),
      '@L[fr]#title' => "Énoncez les arguments à l'appui de votre position.",
      '#required' => TRUE,
    ),
    
    'request_amend_docs' => array(
        '@extends' => 'info_text2',
        'heading' => array(
          '#markup' => 'Are you responding to a request to amend documents under paragraph 33(2)(a) or submit documents whose filing is not provided for in the Rules under paragraph 34(2)(a)?',
          '@L[fr]#markup' => "Répondez-vous à une requête de modification de documents en vertu de l'alinéa 33(2)a) ou à une requête de dépôt de documents non prévu par les Règles en vertu de l'alinéa 34(2)a)? ",
        ),
        'body' => array(
          '#markup' => 'If applicable, describe any prejudice that would be caused if the request were granted, including an explanation of how the proposed amendments or filing would hinder or delay the fair conduct of the proceeding.',
          '@L[fr]#markup' => "Donnez une description, le cas échéant, de tout préjudice qui pourrait vous être causé si la requête était accordée, y compris une explication qui précise comment le dépôt des modifications proposées pourrait entraver ou retarder le déroulement équitable de l'instance.",
        ),
      ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your response, you must file them on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réponse, vous devez les déposer le même jour que votre réponse.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

