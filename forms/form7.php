<?php

// Declare namespace
namespace DF;
// Include useful functions
require_once __DIR__.'/../dynamic_forms_classes.php';

// Declare array building function
function get_form() {
  
  // Declare important PHP variables used by array
  $intro_text = '<h2>Who should use this form?</h2>
<p>An applicant.</p>
<h2>Purpose</h2>
<p>To reply to an answer filed by a respondent.</p>
<p>A reply must not raise issues or arguments that are not addressed in the answer or introduce new evidence.</p>
<p>If you wish to raise new issues or arguments or introduce new evidence, you must first make a request to the Agency in accordance with <a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-7-8">section 34</a> (<a href="https://services.cta-otc.gc.ca/forms" target="_blank">Form 13</a>) and that request must be granted by the Agency. This will delay the processing of the case as, in the interests of fairness, the respondent will be given an opportunity to respond to any new issues, arguments or evidence raised in the reply.</p>
<h2>When should you file this form?</h2>
<p>Within 5 business days after the day on which you receive a copy of the answer, unless another time period is specified by the Agency.</p>
<p>Refer to <a href="http://23.23.154.142/eng/publication/annotated-dispute-adjudication-rules#toc-tm-6-3" target="_blank">section 20</a> of the Dispute Adjudication Rules for more information.</p>
<h2>What happens next?</h2>
<p>Unless other issues arise that need to be addressed, the pleadings close and no further documents or information may be filed with the Agency.</p>
<h2>Collection of personal information</h2>
<p>For more information, please refer to our <a href="http://23.23.154.142/eng/personal-information-collection-statement" target="_blank">Personal Information Collection Statement</a>.</p>';

  $intro_text_fr = "<h2>Qui devrait utiliser ce formulaire?</h2>
<p>Un demandeur.</p>
<h2>But</h2>
<p>Répliquer à une réponse déposée par un défendeur. <strong></strong></p>
<p>Une réplique ne peut soulever des questions ou des arguments qui ne sont pas abordés dans la réponse, ni introduire une nouvelle preuve.</p>
<p>Si vous souhaitez soulever des questions ou des arguments qui ne sont pas abordés dans la réponse, ou introduire une nouvelle preuve, vous devez d’abord présenter une requête à l'Office en vertu de l’<a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-7-8\">article 34</a> (<a href=\"https://services.cta-otc.gc.ca/fra/formulaires\" target=\"_blank\">formulaire 13</a>), et l’Office doit accorder cette requête. Le traitement du dossier sera retardé puisque, par souci d’équité, le défendeur aura la possibilité de répondre à tout nouvel argument ainsi qu’à toute nouvelle question ou preuve soulevée dans la réplique.</p>
<h2>Quand devriez-vous déposer ce formulaire?</h2>
<p>Dans les cinq jours ouvrables suivant la date de réception de la copie de la réponse, à moins que l’Office ne prescrive un autre délai.</p>
<p>Veuillez consulter l’<a href=\"http://23.23.154.142/fra/publication/regles-annotees-pour-le-reglement-des-differends#toc-tm-6-3\" target=\"_blank\">article 20</a> des Règles pour le règlement des différends pour de plus amples renseignements.</p>
<h2>Quelle est la prochaine étape?</h2>
<p>Si aucune autre question ne requiert un examen, l'instance est close et aucun autre document ou renseignement ne peut être déposé auprès de l’Office.&nbsp;</p>
<h2><span>Collecte de renseignements personnels</span></h2>
<p>Veuillez consulter notre <a href=\"http://23.23.154.142/fra/enonce-collecte-renseignements-personnels\" target=\"_blank\">Énoncé sur la collecte de renseignements personnels</a> pour de plus amples renseignements.</p>
<p></p>
</div>";

  
  // Define the first page
  $page1 = array(
    
    // First element
    'into_text' => array(
      // Inherit the properties from the description_text fieldset element
      '@extends' => 'description_text',
      // Override the details from a field element
      '#markup' => $intro_text,
      '@L[fr]#markup' => $intro_text_fr,
    ),
    
    'page_heading' => array(
      // Once again inherit, since it will include appropriate class info
      '@extends' => 'page_heading',
      '#markup' => t('Part 1 of 3: Identification'), 
      '@L[fr]#markup' => 'Partie 1 de 3 : Identification',
    ),
    
    'case_id' => array(
      '@extends' => 'case_id',
    ),
    
    'basic_contact_info' => array(
      '@extends' => 'basic_contact_info',
    ),
  );
  
  
  
  
  // Define the second page
  $page2 = array(
    '#type' => 'group',
    '#title' => t('Details'),
    '@L[fr]#title' => 'Détails',
    '@variables' => array(
      '<form_short_name>' => 'reply to answer',
      '<form_short_name_fr>' => 'réplique à la réponse',
    ),
    
    'page_heading' => array(
      '@extends' => 'page_heading',
      '@#markup' => t('Part 2 of 3: Details of the <form_short_name>'),
      '@L[fr]@#markup' => 'Partie 2 de 3 : Détails relatifs au <form_short_name_fr>',
    ),
    
    'agree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you agree with in the answer.'),
      '@L[fr]#title' => 'Indiquez les éléments dans la réponse avec lesquels vous êtes en accord.',
      '#required' => TRUE,
    ),
    
    'disagree_with' => array(
      '#type' => 'textarea',
      '#title' => t('State what you disagree with in the answer.'),
      '@L[fr]#title' => 'Indiquez les éléments dans la réponse avec lesquels vous êtes en désaccord.',
      '#required' => TRUE,
    ),
    
    'arguments' => array(
      '#type' => 'textarea',
      '#title' => t('Clearly  set out the arguments in support of your reply.'),
      '@L[fr]#title' => "Énoncez clairement les arguments à l'appui de votre réplique.",
      '#required' => TRUE,
    ),
    
    'have_supporting_docs' => array(
      '@extends' => 'have_supporting_docs',
      
      'have_supporting_docs_description' => array(
        '#markup' => 'If you have documents that you are relying on to support your reply, you must file them  on the same day.',
        '@L[fr]#markup' => "Si vous avez des documents à l'appui de votre réplique, vous devez les déposer le même jour que votre réplique.",
      ),
    ),
    
    'list_supporting_docs' => array(
      '@extends' => 'list_supporting_docs',
    ),
   
    'confidential_docs' => array(
      '@extends' => 'confidential_docs',
    ),
    
  );
  
  $page3 = array(
    '@extends' => 'submission_page',
  );
  

  $formArray = array(
    '@overrides' => array(      
      'organization->#weight' => NULL,
    ),
    
  );
  
    
  $formArray['page1'] = $page1;
  $formArray['page2'] = $page2;
  $formArray['page3'] = $page3;

  return $formArray;
}

/**
 * Check requirements
 */
/*
function check_required($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
   
  }
}
 * 
 */

/**
 * Check dependencies
 */
/*
function check_dependencies($arguments) {
  if (isset($arguments['field'])) {
    $field = $arguments['field'];
    $result = $arguments['result'];
  }
}
 * 
 */

