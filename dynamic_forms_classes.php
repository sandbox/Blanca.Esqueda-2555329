<?php

namespace DF;

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once 'forms/field_templates.php';
require_once 'forms/config.forms.php';

/**
 * Generic Form Class
 */
class Form {
  public $pages = array();
  public $page_properties = array();
  public $form_properties = array();
  public $overrides = array();
  
  /**
   * Constructor
   */
  public function __construct(&$formArray, &$form_state, $page_id=NULL) {
        
    // Add all properties and pages
    foreach ($formArray as $name => $codeArray) {
      // Check if property
      if ($name[0] == '@') {
        // Page-level properties
        if ($name == '@properties') {
          // Add properties
          $this->page_properties;
        }
        // Element overrides
        elseif ($name == '@overrides') {
          // Add overrides
          $this->add_overrides($codeArray);
        }
        // Element overrides
        elseif ($name == '@post_submission') {
          // Check for extends
          Form::extend($codeArray);
          // Add post submission page
          $this->form_properties[$name] = $codeArray;
        }
        // Form-level properties
        else {
          $this->form_properties[$name] = $codeArray;
        }
      }
      // Check if page
      else {
        // Set name
        $codeArray['@name'] = $name;
        
        // Add pages
        $this->pages[$name] = Element::build($codeArray, $this, $this);
      }
    }
        
    // Set current and past pages
    $this->set_current_page_info($form_state);
    
    // Add prev, next, and submit buttons (if not submitted)
    if (!Form::is_submitted()) {
      $this->_add_form_buttons($page_id);
    }
  }
  
  /**
   * Create overrides
   */
  public function add_overrides(&$overrides) {
    // Iterate over overrides and add them
    foreach ($overrides as $elem_prop => $value) {
      // Get position of member opperator
      $member_pos = strpos($elem_prop, '->');
      // resolution opperator exists
      if ($member_pos !== FALSE) {
        // Get parent name
        $parrent = substr($elem_prop, 0, $member_pos);
        // Get child name
        $child = substr($elem_prop, $member_pos + 2);
        // Add to override
        if (!isset($this->overrides[$parrent])) {
          $this->overrides[$parrent] = array();
        }        
        $this->overrides[$parrent][$child] = $value;
      }
    }
  }
  
  /**
   * Whether form has been submitted
   */
  public static function is_submitted() {
    if (isset($_SESSION['dynamic_forms']['#form_info']['#submitted'])) {
      return $_SESSION['dynamic_forms']['#form_info']['#submitted'];
    }
    else {
      return FALSE;
    }
  }
  
  /**
   * Set if form has been submitted
   */
  public static function set_submitted($value=TRUE) {
    $_SESSION['dynamic_forms']['#form_info']['#submitted'] = $value;    
  }
  
  
  /**
   * Set form pages
   */
  public function set_current_page_info(&$form_state) {
    // On submit call...
    if (isset($form_state['triggering_element']['#type']) AND isset($form_state['triggering_element']['#name']) AND $form_state['triggering_element']['#type'] == 'submit') {
      // Check for previoius button trigger
      if ($form_state['triggering_element']['#name'] == 'prev') {
        $this->set_prev_page();
      }
      // Check for next button trigger
      elseif ($form_state['triggering_element']['#name'] == 'next') {
        $this->set_next_page();
      }    
    }
  }
  
  /**
   * Render the page
   */
  public function render($page_id=NULL) {       
    // If form is submitted, render complete form
    if (Form::is_submitted()) {
      // Declare page array
      $page_array = array();
      // Build all pages
      foreach ($this->pages as $page_name => $page_obj) {
        // Return rendered FAPI array
        $page_array[$page_name] = $page_obj->render();
      }
    }
    else {
      // if $page is not set or does not exist...
      if (empty($page_id) OR !isset($this->pages[$page_id])) {
        // Get active page
        $page_id = $this->get_active_page();
      }
      // Return rendered FAPI array
      $page_array = $this->pages[$page_id]->render();
    }
    
    // Wrap id around page
    $this->wrap_page($page_array);
    
    // Return rendered array
    return $page_array;
  }
  
  /**
   * Adds inheritance
   */
  public static function extend(&$childCodeArray) {
    
    // Check if code requires extending
    if (isset($childCodeArray['@extends'])) {
      // Get template name
      $template_name = convert_placeholders($childCodeArray['@extends']);
      // Check if template exists
      if (isset(templates::$field_templates[$template_name])) {
        // Get code to inherite
        $parentCodeArray = templates::$field_templates[$template_name];
        // Check if inheritable code also requires extending
        if (isset($parentCodeArray['@extends'])) {
          Form::extend($parentCodeArray);
        }
        // Merge child code with base (parent)
        $childCodeArray = array_replace_recursive($parentCodeArray, $childCodeArray);
      }
    }
  }
  
  
  /**
   * 
   * @param type $wrap_pageWrap form id around page
   */
  public function wrap_page(&$wrap_page) {
    if (isset($wrap_page['#prefix'])) {
      $wrap_page['#prefix'] = '<div id="dynamic-forms">' . $wrap_page['#prefix'];
    }
    else {
      $wrap_page['#prefix'] = '<div id="dynamic-forms">';
    }
    if (isset($wrap_page['#suffix'])) {
      $wrap_page['#suffix'] .= '</div>';
    }
    else {
      $wrap_page['#suffix'] = '</div>';
    }    
  }
  
  /**
  * Set active page
  */
  public function set_active_page($active_page) {
    //$_SESSION['dynamic_forms']['#form_info']['#active_page'] = $active_page;
    set_system_variable('@active_page', $active_page);
  }

  /**
   * Get active page
   */
  public function get_active_page() {
    // If active page previously set then return it
    /*
    if (isset($_SESSION['dynamic_forms']['#form_info']['#active_page'])) {
      return $_SESSION['dynamic_forms']['#form_info']['#active_page'];
    }
     */
    if (check_system_variable('@active_page')) {
      return get_system_variable('@active_page');
    }
    // If active page not previously set
    else {
      // Get first page
      $active_page = $this->get_first_page();
      // Set active page
      $this->set_active_page($active_page);
      // Retunr active page
      return $active_page;
    }
  }
  
  
  /**
   * Get next page
   */
  public function get_next_page() {
    // Get active page
    $active_page = $this->get_active_page();
    // Get next active page
    $active_page_found = FALSE;
    foreach ($this->pages as $page_id => $page_obj) {
      // Flag when active page is found in $page array
      if ($page_id == $active_page) {
        $active_page_found = TRUE;
      }
      // Get next active page that passes dependencies
      elseif ($active_page_found AND $this->check_page_dependencies($page_obj)) {
        return $page_id;
      }      
    }
  }
  
  /**
   * Set next page
   */
  public function set_next_page() {
    $this->set_active_page($this->get_next_page());
  }
  
  /**
   * Get next page
   */
  public function get_prev_page() {
    // Get active page
    $active_page = $this->get_active_page();
    
    // Get next active page
    $active_page_found = FALSE;
    // Set array cursor to end
    end($this->pages);
    // Reverse iterate over the pages
    while($page_obj = current($this->pages)) {
      
      // Get page id
      $page_id = key($this->pages);
      // Flag when active page is found in $page array
      if ($page_id == $active_page) {
        $active_page_found = TRUE;
      }
      // Get next active page that passes dependencies
      elseif ($active_page_found AND $this->check_page_dependencies($page_obj)) {
        return $page_id;
      }
      
      // Get previoius page
      prev($this->pages);
    }
  }
  
  /**
   * Set next page
   */
  public function set_prev_page() {
    $this->set_active_page($this->get_prev_page());
  }
  
  
  /**
   * Check if a reference page is the first page that passes dependencies
   */
  public function is_first_page($ref_page_id) {
    foreach ($this->pages as $page_id => $page_obj) {
      // Get first page that passes dependencies and check if it matches...
      if ($this->check_page_dependencies($page_obj)) {
        return ($ref_page_id == $page_id);
      }
    }
    // If ref page cannot be found
    return false;
  }
  
  /**
   * Check if a reference page is the last page that passes dependencies
   */
  public function is_last_page($ref_page_id) {
    // Set array cursor to end
    end($this->pages);
    // Reverse iterate over the pages
    while($page_obj = current($this->pages)) {
      // Check if page passes dependencies
      if ($this->check_page_dependencies($page_obj)) {
        // Get page id
        $page_id = key($this->pages);
        // Return whether it is a match
        return ($ref_page_id == $page_id);
      }
      // Get previoius page
      prev($this->pages);
    }
    // If page cannot be found
    return false;
  }
  
  /**
   * Get first page
   */
  public function get_first_page() {
    // Iterate over pages
    foreach ($this->pages as $page_id => $page_obj) {
      // Get first page that passes dependencies and return it
      if ($this->check_page_dependencies($page_obj)) {
        return $page_id;
      }
    }
  }
    
  /**
   * Check if page dependencies are passed
   */
  public function check_page_dependencies(Element &$page) {
    // Check if dependencies are specified
    return $page->checkPropertyConditions('@dependencies');
  }
  
  /**
   * Add previous, next, and submit buttons
   */
  public function _add_form_buttons($page_id) {
    
    // if $page is not set or does not exist...
    if (empty($page_id) OR !isset($this->pages[$page_id])) {
      // Get active page
      $page_id = $this->get_active_page();
    }
    
    // Get active page
    $current_page =& $this->pages[$page_id];
    
    // Add span wrapper
    $submit_code = array(
      '#type' => 'group',
      '@wrapper_tag' => 'span',
      '@name' => 'submit_buttons',
    );
    
    // If not first page
    if (!$this->is_first_page($page_id)) {
      // Set button name
      $name = 'prev';
      // Check if field exists in template
      if (isset(templates::$field_templates[$name])) {
        // Add previous button
        $code = templates::$field_templates[$name];
      }
      // If does not exist, use default
      else {
        // Add previous button
        $code = array(
          '#type' => 'submit',
          '#value' => t('Previous Page'),
          '@L[fr]#value' => 'Page précédente',
          '#attributes' => array('class' => array('df-prev')),
          '#executes_submit_callback' => FALSE,
          '#limit_validation_errors' => array(),
          '#name' => 'prev',
        );
      }
      // Add name
      //$code['@name'] = $name;
      // Add to span
      $submit_code[$name] = $code;
      // Build element      
      //$current_page->elements[$name] = Element::build($code, $current_page, $this);
    }
    
    // If not last page
    if (!$this->is_last_page($page_id)) {
      // Set button name
      $name = 'next';
      // Check if field exists in template
      if (isset(templates::$field_templates[$name])) {
        // Add next button
        $code = templates::$field_templates[$name];
      }
      // If does not exist, use default
      else {
        // Add next button
        $code = array(
          '#type' => 'submit',
          '#value' => t('Submit'),
          '@L[fr]#value' => 'Soumettre',
          '#attributes' => array('id' => array('edit-submit')),  
          '#executes_submit_callback' => TRUE,
          //'#submit' => array('dynamic_forms_submit'),
        );
      }
      // Add name
      //$code['@name'] = $name;
      // Add to span
      $submit_code[$name] = $code;
      // Build element
      //$current_page->elements[$name] = Element::build($code, $current_page, $this);
    }
    // If is last page
    else {
      // Set button name
      $name = 'submit';
      // Check if field exists in template
      if (isset(templates::$field_templates[$name])) {
        // Add submit button
        $code = templates::$field_templates[$name];
      }
      // If does not exist, use default
      else {
        // Add submit button
        $code = array(
          '#type' => 'submit',
          '#value' => t('Submit'),
          '@L[fr]#value' => 'Soumettre',
          '#attributes' => array('id' => array('edit-submit')),
        );
      }      
      // Add name
      //$code['@name'] = $name;
      // Add to span
      $submit_code[$name] = $code;
      // Build element
      //$current_page->elements[$name] = Element::build($code, $current_page, $this);
    }
    
    // Build element
    $current_page->elements[$name] = Element::build($submit_code, $current_page, $this);
  }
}


/**
 * Generic Element Class
 */
class Element {

  public $name = '';
  public $elements = array();
  public $value = '';
  public $type;
  public $attributes = array();
  public $properties = array();
  public $variables = array();
  public $component_properties = array();
  public $component_of = array();
  public $codeArray;
  public $dependencies = array();
  public $required_by = array();
  public $translations = array();
  public static $id_counter = 0;
  public static $default_lang = 'eng';
  public static $lang_list = array('eng', 'fra');
  public static $form_fields = array();
  public static $required_labels = array();

  /**
   * Element Constructor
   */
  public function __construct() {
    // Create array for language translation
    foreach (Element::$lang_list as $lang) {
      if ($lang != Element::$default_lang) {
        $this->translations[$lang] = array();
      }
    }
  }

  /**
   * Builds a Element Instance (Factory Function)
   */
  public static function build(array $fieldCodeArray, &$parentElementObj=NULL, &$formObj=NULL) {

    // Set evaluation time to 'B'uild
    //set_system_variable('eval_time', 'B');
    
    // Get node language
    global $language;
    $user_language = $language->language;
    
    // Create new element object
    $elementObj = new Element();
    
    // Set current element object
    set_current_element($elementObj);
    
    // Set parent element
    $elementObj->component_of = &$parentElementObj;

    // Set overrides
    if (!empty($formObj->overrides)) {      
      if (!empty($formObj->overrides[$fieldCodeArray['@name']])) {
        foreach ($formObj->overrides[$fieldCodeArray['@name']] as $override => $value) {
          $fieldCodeArray[$override] = $value;
        }
      }
    }
    
    // If root element
    if ($parentElementObj == NULL OR is_a($parentElementObj, 'DF\Form')) {
      // Check if any fields have been inherited
      Element::extend($fieldCodeArray);
    }

    // Save codeArray representation
    $elementObj->codeArray = $fieldCodeArray;

    // Verify session storage is set
    $elementObj->_init_storage();
    
    // Set variables
    if (isset($fieldCodeArray['@variables']) AND is_array($fieldCodeArray['@variables'])) {
      // Set variables
      foreach ($fieldCodeArray['@variables'] as $var => $value) {
        // Set variable
        set_variable($var, $value, $elementObj, TRUE);
      }
    }
    // Init variables
    if (isset($fieldCodeArray['@init_variables']) AND is_array($fieldCodeArray['@init_variables'])) {
      // Init variables
      foreach ($fieldCodeArray['@init_variables'] as $var => $value) {
        // If global variable
        if ($var[0] == '<' and substr($var, -1) == '>') {
          init_global_variable(substr($var, 1, -1), $value);
        }
        // If local variable
        elseif ($var[0] == '[' and substr($var, -1) == ']') {
          //init_local_variable(substr($var, 1, -1), $value, $elementObj);
          // TODO: Implement init and set local variables
          // TODO: Implement local variables in session
        }
        // Set default variable type (aka, global)
        else {
          init_global_variable($var, $value);
        }
      }
    }


    // Add inheritables to property list
    if (!empty($elementObj->component_of)) {
      
      // Add previously inherited prefix 
      if (isset($elementObj->component_of->properties['@inherited_prefix'])) {
        $value = $elementObj->component_of->properties['@inherited_prefix'];
        $elementObj->prepend_property('@inherited_prefix', $value);
      }
      // Add inheritable prefix
      if (isset($elementObj->component_of->properties['@inheritable_prefix'])) {
        $value = $elementObj->component_of->properties['@inheritable_prefix'];
        $elementObj->prepend_property('@inherited_prefix', $value);
      }
      
      // Add previously inherited suffixes 
      if (isset($elementObj->component_of->properties['@inherited_suffix'])) {
        $value = $elementObj->component_of->properties['@inherited_suffix'];
        $elementObj->append_property('@inherited_suffix', $value);
      }
      // Add inheritable suffix
      if (isset($elementObj->component_of->properties['@inheritable_suffix'])) {
        $value = $elementObj->component_of->properties['@inheritable_suffix'];
        $elementObj->append_property('@inherited_suffix', $value);
      }
    }    

    

    // Declarations
    $properties = array();
    $subelements = array();

    // Organize code into attributes, translations, properties, and subelements
    foreach ($fieldCodeArray as $key => $code) {
      // Organize attributes
      if ($key[0] == '#') {
        $elementObj->attributes[$key] = $code;
      }
      // Add a dynamics forms property/attribut
      elseif ($key[0] == '@') {
        if ($key == '@inherited_prefix') {
          $elementObj->prepend_property('@inherited_prefix', $code);
        }
        elseif ($key == '@inherited_suffix') {
         $elementObj->append_property('@inherited_suffix', $code);
        }
        // Organize translations
        elseif (substr($key, 1, 2) == 'L[') {
          // Get position of attribute/property
          $pos = strpos($key, ']');
          if ($pos AND $pos > 4) {
            // Get language
            $property_lang = substr($key, 3, $pos - 3);
            // Get translated attribute
            $attribute = substr($key, $pos+1);
            // Create translation, which is stored in $value
            $elementObj->translations[$property_lang][$attribute] = $code;
            // Reserve array order position if correct language
            if ($property_lang == $user_language) {
              if ($attribute[0] == '#') {
                // Reserve attribute location
                $elementObj->attributes[$attribute] = '';
              }
              elseif ($attribute[0] == '@') {
                // Reserver property location
                $properties[$attribute] = '';
                // Check if variable setting                
              }
              else {
                // Reserve subelement location
                $subelements[$attribute] = '';
              }
            }
          }
        }
        // Organize properties
        else {
          $properties[$key] = $code;
        }
      }
      // build subgroup or element
      else {
        // check if element is valid
        if (!is_string($code)) {
          // Organize subelements
          $subelements[$key] = $code;
        }
        else {
          // Set error
          $stmt = "<h2>Dynamic Forms Error Checker</h2><b>Invalid Statement:</b><br> '" . $key . "' => '" . $code . "'";
          if (isset($parentElementObj) AND isset($parentElementObj->name)) {
            $stmt .= "<br><b>Within element:</b><br> '" . $parentElementObj->name . "'";
          }
          if (isset($_SESSION['dynamic_forms']['#form_info']['#current_form_url'])) {
            $stmt .= "<br><b>Within form:</b><br>'" 
                     . $_SESSION['dynamic_forms']['#form_info']['#current_form_url']
                     . "'";
          }
          //df_error_message($stmt, 'error');
          drupal_set_message($stmt, 'warning');
        }
      }
    }

    // Build translations and replace relevant attributes, properties, and subelements    
    if (isset($elementObj->translations[$user_language])) {
      foreach ($elementObj->translations[$user_language] as $key => $value) {
        if ($key[0] == '#') {
          $elementObj->attributes[$key] = $value;
        }
        elseif ($key[0] == '@') {
          $properties[$key] = $value;
        }
        else {
          $subelements[$key] = $value;
        }
      }
    }

    
    // Build properties
    foreach ($properties as $key => $code) {
      if ($key == '@ajax_send') {
        // Only add ajax call if one is not already defined
        if ($code == TRUE) {
          if (!isset($elementObj->attributes['#ajax'])) {
            $elementObj->attributes['#ajax'] = array();
          }
          if (!isset($elementObj->attributes['#ajax']['callback'])) {
            $elementObj->attributes['#ajax']['callback'] = 'dynamic_forms_generator_callback';
          }
          if (!isset($elementObj->attributes['#ajax']['effect'])) {
            $elementObj->attributes['#ajax']['effect'] = 'none';
          }
        }
      }
      // Do not evaluate dependencies during build time (check during render time)
      elseif ($key == '@dependencies') {
        $elementObj->properties[$key] = $code;
      }
      // Build all other dynamic forms propertie
      else {
        $elementObj->properties[$key] = interpret_structure($code, $elementObj);
      }
    }
    
    // Set name
    if (isset($fieldCodeArray['@name'])) {
      $elementObj->setName(convert_placeholders($fieldCodeArray['@name']), $elementObj);
    }
    else {
      $elementObj->setName('main-form');
    }
    
    // Do not render buttons after submit
    /*
    if (Form::is_submitted()) {
      if (isset($fieldCodeArray['#type']) AND ($fieldCodeArray['#type'] == 'submit' OR $fieldCodeArray['#type'] == 'button')) {
        $elementObj->attributes = array('#markup' => '');
        $elementObj->properties = array('@name' => $elementObj->name);
        $elementObj->elements = array();
        return $elementObj;
      }
      elseif (isset($fieldCodeArray['@name']) AND $fieldCodeArray['@name'] == 'into_text') {
        // DO SOMETHING HERE IF THE INTRO TEXT IS NOT WISHED TO BE INCLUDED
      }
      // Disable field
      //$fieldCodeArray['#disabled'] = TRUE;
    }
     * 
     */
    

    // Build subelements
    foreach ($subelements as $key => $code) {       
      // Check if any fields have been inheritanted
      Element::extend($code);
      // Check for build loop
      if (isset($code['@build_loop']) AND isset($code['@build_loop']['start']) AND isset($code['@build_loop']['end']) AND isset($code['@build_loop']['iterator_variable'])) {

        // TODO: add element to convert_placeholders !!!
        
        // Get variables
        $start = trim(convert_placeholders($code['@build_loop']['start']));
        $end = trim(convert_placeholders($code['@build_loop']['end']));
        $iterator_variable = trim($code['@build_loop']['iterator_variable']);
        // Set safety max loops
        if (isset($code['@build_loop']['max_loops'])) {
          $max_loops = $code['@build_loop']['max_loops'];
        }
        else {
          $max_loops = 10;
        }
        
        // Add default variable time (local)
        if ($iterator_variable[0] != '[' AND $iterator_variable[0] != '<') {
          $iterator_variable = '[' . $iterator_variable . ']';
        }
        
        // Get inherited suffix
        $inherited_suffix = $iterator_variable;
        
        // Add build time eval time to inherited suffix if not contained
        if (substr($iterator_variable, 1, 2) != 'B:') {
          $inherited_suffix = $inherited_suffix[0] . 'B:' . substr($inherited_suffix, 1);
        }
        
        // Add inherited name suffix        
        if (!isset($code['@inherited_suffix'])) {
          $code['@inherited_suffix'] = $inherited_suffix;
        }
        else {
          $code['@inherited_suffix'] .= $inherited_suffix;
        }

        // Init counters 
        $counter = $start;
        $saftyCounter = 0;

        // Run loop
        while ($counter <= $end) {

          // To prevent infinite (and large) loops
          $saftyCounter++;
          if ($saftyCounter > $max_loops) {
            break;
          }

          // Save counter
          set_variable($iterator_variable, $counter, $elementObj, TRUE);
          /*
          // Check global variable
          if (isset($iterator_variable[0]) AND $iterator_variable[0] == '<' AND substr($iterator_variable, -1) == '>') {
            set_global_variable($iterator_variable, $counter);
          }
          // Check local variable
          elseif (isset($iterator_variable[0]) AND $iterator_variable[0] == '[' AND substr($iterator_variable, -1) == ']') {
            $code['@variables'][substr($iterator_variable, 1, -1)] = $counter;
          }
          // Check default case (local)
          else {
            $code['@variables'][$iterator_variable] = $counter;
          }
           * 
           */
          
          // Add name if not set
          if (!isset($code['@name'])) {
            $code['@name'] = $key;
          }
          
          // Build element
          $element = Element::build($code, $elementObj, $formObj);
          $elementObj->addElement($element);
          // Increase counter
          $counter++;
          // Set current element object
          set_current_element($elementObj);
        }
      }
      else {
        // Add name if not set
        if (!isset($code['@name'])) {
          $code['@name'] = $key;
        }
        

        // Build element
        $element = Element::build($code, $elementObj, $formObj);
        $elementObj->addElement($element);
        // Set current element object
        set_current_element($elementObj);
      }
    }

    // Add wrapper
    // TODO: Add code to only wrap fields that might be updated
    $elementObj->_add_wrapper();

    // Set fieldset required label
    $elementObj->correct_fieldset_required_maker();
    
    // Add AJAX
    //$elementObj->_add_ajax();

    // Add element to storage
    //set_form_object($elementObj);

    // Setup default values
    $elementObj->_setup_default_value();

    // Add reference to element
    //Element::$form_fields[$elementObj->getName()] = 
    // Set evaluation time to 'O'ther
    //set_system_variable('eval_time', 'O');

    // Return new element
    return $elementObj;
  }
  
  /**
   * Corrects required marker on fieldsets that contain checkboxes and radio buttons
   */
  public function correct_fieldset_required_maker() {
    // Check if it is a fieldset
    if (isset($this->attributes['#type']) AND $this->attributes['#type'] == 'fieldset') {
      // Check if only contains one subelement
      if (isset($this->elements) AND count($this->elements) == 1) {
        // Get child
        $child = reset($this->elements);
        
        // Check if element is a checkbox or radio button, is required, and does not have title set
        // Check if child type is set
        if (isset($child->attributes['#type'])) {
          // Check if its a radio or checkbox
          if ($child->attributes['#type'] == 'checkboxes' OR $child->attributes['#type'] == 'radios') {
            // Check if required is true
            if (isset($child->attributes['#required']) AND $child->attributes['#required'] == TRUE) {
              // Unset title if set but empty
              if (isset($child->attributes['#title']) AND empty($child->attributes['#title'])) {
                unset($child->attributes['#title']);
              }              
              // Check if title is unset
              if (!isset($child->attributes['#title'])) {
                // Translate tool tip
                $t_tool_tip = t('This field is required.');
                // Set required marker
                $this->attributes['#title'] .= ' <span class="form-required badge" title="' . $t_tool_tip . '">Required</span>'; 
                // Set required
                $this->attributes['#required'] = TRUE;
                // Set custom label if set
                if (isset($child->properties['@required_label'])) {
                  $this->properties['@required_label'] = $child->properties['@required_label'];
                }                
              }              
            }
                    
          }
        }
            
        
        if ((isset($child->attributes['#type']) AND ($child->attributes['#type'] == 'checkboxes' OR $child->attributes['#type'] == 'radios')) AND
            (isset($this->attributes['#required']) AND $this->attributes['#required'] == TRUE)) {
          
        }
      }
    }
    
  }
  
  
  /**
   * Appends a property
   */
  public function append_property($property, $value, $separator='_') {
    if (isset($this->properties[$property])) {
      $this->properties[$property] .= $separator . $value;
    }
    else {
      $this->properties[$property] = $value;
    }
  }
  
  /**
   * Prepends a property
   */
  public function prepend_property($property, $value, $separator='_') {
    if (isset($this->properties[$property])) {
      $this->properties[$property] = $value . $separator . $this->properties[$property];
    }
    else {
      $this->properties[$property] = $value;
    }
  }

  /**
   * Adds inheritance
   */
  public static function extend(&$childCodeArray) {
    
    // Check if code requires extending
    if (isset($childCodeArray['@extends'])) {
      // Get template name
      $template_name = convert_placeholders($childCodeArray['@extends']);
      // Check if template exists
      if (isset(templates::$field_templates[$template_name])) {
        // Get code to inherite
        $parentCodeArray = templates::$field_templates[$template_name];
        // Check if inheritable code also requires extending
        if (isset($parentCodeArray['@extends'])) {
          Element::extend($parentCodeArray);
        }
        // Merge child code with base (parent)
        $childCodeArray = array_replace_recursive($parentCodeArray, $childCodeArray);
      }
    }
  }

  
  /**
   * Add div wrapper and appropriate ID
   */
  function _add_wrapper() {

    // Create wrapper name
    $wrapper_name = 'dynamic-' . str_replace('_', '-', $this->name);
    // Save wrapper name
    $this->properties['@wrapper_name'] = $wrapper_name;
        
    // Check if wrapper tag type is set
    if (isset($this->properties['@wrapper_tag'])) {
      // Get wrapper tag type
      $wrapper_tag = $this->properties['@wrapper_tag'];
      // If set to false, don't wrap element
      if ($wrapper_tag === FALSE) {
        return;
      }
    }
    
    // Check what type of element it is
    $input_types = array('actions', 'button', 'checkbox', 'checkboxes', 'container', 'date', 'fieldset', 'file', 'form', 'image_button', 'password', 'radio', 'radios', 'select', 'submit', 'tableselect', 'textarea', 'text_format', 'textfield', 'weight');
    if (empty($wrapper_tag) AND isset($this->attributes['#type']) AND in_array($this->attributes['#type'], $input_types)) {
      $this->_add_wrapper_attrib($wrapper_name);
    }
    else {
      $this->_add_wrapper_pre_suffix($wrapper_name);
    }
    
  }
  
  /**
   * Add attribute wrapper
   */
  function _add_wrapper_attrib($wrapper_name=NULL) {
    if (empty($wrapper_name) AND !empty($this->properties['@wrapper_name'])) {
      $wrapper_name = $this->properties['@wrapper_name'];
    }    
    
    if (!empty($wrapper_name)) {
      if (isset($this->attributes['#attributes']['class']) AND is_array($this->attributes['#attributes']['class'])) {
        $this->attributes['#attributes']['class'][] = $wrapper_name;
      }
      else {
        $this->attributes['#attributes']['class'] = array($wrapper_name);
      }
    }
  }

  /**
   * Add attribute wrapper
   */
  function _add_wrapper_pre_suffix($wrapper_name=NULL , $wrapper_tag=NULL) {
    // Get wrapper name
    if (empty($wrapper_name) AND !empty($this->properties['@wrapper_name'])) {
      $wrapper_name = $this->properties['@wrapper_name'];
    }
    
    // Check if wrapper tag type is set
    if (empty($wrapper_tag)) {
      if (isset($this->properties['@wrapper_tag'])) {        
        // Get wrapper tag type
        $wrapper_tag = $this->properties['@wrapper_tag'];
        // If set to false, don't wrap element
        if ($wrapper_tag === FALSE) {
          return;
        }
      }
      else {        
        // Set same tag flag
        $tags_same = TRUE;
      }
    }
    
    // Set default wrapper tag if necessary
    if (empty($wrapper_tag)) {
      // Set default wrapper tag type    
      $wrapper_tag = 'div';
    }

    // Set flag
    $class_found = FALSE;
    
    // Check if wrapper tags are same
    if (isset($this->attributes['#prefix'])) {
      $current_tag = trim($this->attributes['#prefix']);
      $t_len = strlen($wrapper_tag);
      if (substr($current_tag, 1, $t_len) == $wrapper_tag) {
        // Set flag
        $tags_same = TRUE;
      }
    }
    
    if ($this->name == 'body' or $this->name == 'how-to-fill-multi') {
      $asf = 'stop';
    }
    
    // If prefix exists, add it to class
    if (isset($this->attributes['#prefix'])) {
      if (isset($tags_same)) {
        // Get current prefix
        $current_prefix = $this->attributes['#prefix'];
        // Get position of class=
        $class_pos = strpos($current_prefix, 'class=');
        // If class= is found
        if ($class_pos !== FALSE) {
          // Find the quote right after the the class=
          $s_quote_pos = strpos($current_prefix, "'", $class_pos+6);
          $d_quote_pos = strpos($current_prefix, '"', $class_pos+6);
          if ($s_quote_pos === FALSE AND $d_quote_pos === FALSE) {
            $quote_pos = NULL;
          }
          elseif ($s_quote_pos !== FALSE AND $d_quote_pos === FALSE) {
            $quote_pos = $s_quote_pos;
          }
          elseif ($s_quote_pos === FALSE AND $d_quote_pos !== FALSE) {
            $quote_pos = $d_quote_pos;
          }
          elseif ($s_quote_pos !== FALSE AND $d_quote_pos !== FALSE) {
            $quote_pos = min(array($s_quote_pos, $d_quote_pos));
          }
        }
        // If quote was found
        if (isset($quote_pos)) {
          // Set found flag
          $class_found = TRUE;

          $new_prefix = substr($current_prefix, 0, $quote_pos+1) . $wrapper_name . ' ' . substr($current_prefix, $quote_pos+1);

          // Add $wrapper_name class to class list
          $this->attributes['#prefix'] = $new_prefix;
          //$this->attributes['#prefix'] .= '<div class="' . $wrapper_name . '">';
        }
        // If quote not found
        else {
          // Preprend prefix
          $this->attributes['#prefix'] = '<' . $wrapper_tag . ' class="' . $wrapper_name . '">' . $this->attributes['#prefix'];
        }
      }
      else {
        // Preprend prefix
        $this->attributes['#prefix'] = '<' . $wrapper_tag . ' class="' . $wrapper_name . '">' . $this->attributes['#prefix'];
      }
    }
    else {
      // Set prefix
      $this->attributes['#prefix'] = '<' . $wrapper_tag . ' class="' . $wrapper_name . '">';
    }

    // Only add suffix if class wasn't found aka added extra tag
    if (!$class_found) {
      // If suffix exists
      if (isset($this->attributes['#suffix'])) {
        // Append suffix
        $this->attributes['#suffix'] .= '</' . $wrapper_tag . '>';
      }
      else {
        // Set suffix
        $this->attributes['#suffix'] = '</' . $wrapper_tag . '>';
      }
    }
  }

  /**
   * Add AJAX attributes
   */
  function _add_ajax() {
    // If ajax attribute not set, then set it
    if (!isset($this->attributes['#ajax'])) {
      $this->attributes['#ajax'] = array();
    }
    // If callback is not set, set it
    if (!isset($this->attributes['#ajax']['callback'])) {
      $this->attributes['#ajax']['callback'] = 'dynamic_forms_generator_callback';
    }
    // If effect is not set, then set it
    if (!isset($this->attributes['#ajax']['callback'])) {
      $this->attributes['#ajax']['effect'] = 'none';
    }
  }

  /**
   * Setup session
   * 
   * Make sure default values session is initialized
   */
  function _init_storage() {
    init_storage();
  }

  /**
   * Setup defualt values
   */
  function _setup_default_value() {    
    // If default value is set
    if (isset($this->attributes['#default_value'])) {
      set_default_value($this->name, $this->attributes['#default_value']);
    }
    // If no default value attribute, use options array if available
    elseif (isset($this->attributes['#options']) AND isset($this->attributes['#options'][0])) {
      $option_keys = array_keys($this->attributes['#options']);
      set_default_value($this->name, $option_keys[0]);
    }
    // Blank for fieldsets
    //elseif (isset($this->attributes['#default_value']) AND $this->attributes['#default_value'] == 'textbox')
  }

  /**
   * Renders Dynamic Form Element to Form API Field Element
   */
  public function render() {

    // Set evaluation time to 'R'ender
    set_system_variable('eval_time', 'R');
    
    // Set current element
    set_current_element($this);
    
    /*
    if (Form::is_submitted()) {
      if (isset($this->attributes['#type']) AND ($this->attributes['#type'] == 'submit' OR $this->attributes['#type'] == 'button')) {
        // DO SOMETHING HERE IF THE INTRO TEXT IS NOT WISHED TO BE INCLUDED
      }
      elseif (isset($fieldCodeArray['@name']) AND $fieldCodeArray['@name'] == 'into_text') {
        // DO SOMETHING HERE IF THE INTRO TEXT IS NOT WISHED TO BE INCLUDED
      }
      // Disable field
      //$fieldCodeArray['#disabled'] = TRUE;
    }
     * 
     */

    // Check if there are either no dependencies or if there are, are they met
    if ($this->checkPropertyConditions('@dependencies')) {

      $field = $this->attributes;

      // Set dynamic properties
      foreach ($this->properties as $property => $value) {
        if (substr($property, 0, 2) == '@#') {
          // Get FAPI attribute
          $attribute = substr($property, 1);
          // Evaluation dynamic structure
          $field[$attribute] = interpret_structure($value);
        }
        elseif ($property == '@required_label') {
          // If wrapper name is set
          if (!empty($this->properties['@wrapper_name'])) {
            // Get class tag
            $input_types = array('actions', 'button', 'checkbox', 'checkboxes', 'date', 'file', 'form', 'image_button', 'password', 'radio', 'radios', 'select', 'submit', 'tableselect', 'textarea', 'text_format', 'textfield', 'weight');
            if (isset($this->attributes['#type']) AND in_array($this->attributes['#type'], $input_types)) {
              $class_tag = '.' . str_replace('dynamic-', 'form-item-', $this->properties['@wrapper_name']);
            }
            else {
              $class_tag = '.' . $this->properties['@wrapper_name'];
            }            
            // Save custom required label (later set by jQuery)
            Element::$required_labels[$class_tag] = $value;
          }
        }
      }
      
      // Call required hooks
      Element::hook_set_attribute($this->name, '#required', $field);
      

      // Iterate over each subelement
      foreach ($this->elements as $name => $subElementObj) {
        // Recursively render subelement object into Forms API array
        $subElement = $subElementObj->render();
        // If Forms API array...
        if (isset($subElement) AND is_array($subElement)) {
          // If the current element is a fieldset, put subelement in fieldset form
          if ($this->type == 'fieldset') {
            $field[$this->name][$name] = $subElement;
          }
          // If current element is not a fieldset, do not put in fieldset form
          else {
            $field[$name] = $subElement;
          }
        }
      }
      // Add element to session storage  
      //set_element_structure($this->getName(), $field);
    }
    // If failed dependency
    else {
      /*
      $field = array(
        '#markup' => $this->attributes['#prefix'] . $this->attributes['#suffix'],
      );
      // Add element to session storage
     set_element_structure($this->getName(), $field);
       * 
       */
      $field = NULL;
    }
    
    // Set previously submitted as default values for the field
    if (!empty($this->name) AND check_value($this->name)) {
      $default_value = get_value($this->name);
      if (!empty($default_value)) {
        $field['#default_value'] = $default_value;
        //$field['#value'] = $default_value;
      }
    }
    

    // Set evaluation time to 'O'ther
    //set_system_variable('eval_time', 'O');

    // Return rendered field
    return $field;
  }

  /**
   * Adds an element to the group
   */
  public function addElement(Element &$element) {

    // Make reference to this element from within new subelement
    $element->component_of =& $this;

    // If name is set
    if (($element->getName() != FALSE) AND ($element->getName() != '')) {
      // Add element to component array
      $this->elements[$element->getName()] = $element;
    }
    // If name not set, generate a name
    else {
      $this->elements['element' . Element::$id_counter++] = $element;
    }
  }

  // Return an incremented name
  public static function incrementName($name, $checkName = TRUE) {
    // Get length of name in characters
    $str_length = strlen($name);
    // Negative counter
    $digit_location = $str_length - 1;
    // Last non-zero numberic location
    $last_nonzero = $str_length;
    // Count how many of the end characters are digits
    while (isset($name[$digit_location]) AND ($name[$digit_location] >= '0' AND $name[$digit_location] <= '9')) {
      if ($name[$digit_location] != '0') {
        // Store last non-zero digit.  Used to handle 0001 and 0000 condition
        $last_nonzero = $digit_location;
      }
      $digit_location--;
    }
    $digit_location++;
    // If there are no digits at the end of the name...
    if ($digit_location == $str_length) {
      // Add a number to the end of name
      $name .= '1';
    }
    // If there are numbers at the end of the name...
    else {
      // Corrent non-zero condition
      if ($last_nonzero == $str_length) {
        $last_nonzero--;
      }
      if ($name[$last_nonzero] == '9') {
        $last_nonzero--;
      }
      if ($last_nonzero < $digit_location) {
        $last_nonzero = $digit_location;
      }
      // Get the letter part of the name
      $prefix = substr($name, 0, $last_nonzero);
      // Get suffix
      $suffix = substr($name, $last_nonzero);
      // Add one to the suffix
      $suffix += 1;
      // Recombine the prefix and the incremented suffix
      $name = $prefix . $suffix;
    }
    // Check if name exists
    if ($checkName AND check_form_name($name)) {
      // Generate new name
      Element::incrementName($name);
    }
    // Return the generated name
    return $name;
  }

  /*
  // Add copy of...
  public static function buildCopy($elementName) {
    $elementObj = get_form_object($elementName);
    $codeArray = $elementObj->codeArray;
    $codeArray['@name'] = Element::incrementName($codeArray['@name'], FALSE);
    return Element::build($codeArray);
  }

  // Clone an element and all subelements
  public function __clone() {
    // Fix name
    $this->name = Element::incrementName($this->name);
    // Setup default value
    $this->_setup_default_value();
    // Hold on to previous elements array
    $previousElements = $this->elements;
    // Clear new elements array
    $this->elements = array();
    // Iterate over previous elements and create deep copy
    foreach ($previousElements as $key => $previousElement) {
      // Clone previous element
      $newElement = clone $previousElement;
      // Setup default values of new element
      $newElement->_setup_default_value();
      // Add new element to the elements to this container
      $this->addElement($newElement);
    }
    // Add element to storage
    set_form_object($this);
  }
   * 
   */

  // Interprets capability
  public function interpret_dynamic_code($structure) {
    return interpret_structure($structure);
  }

  // Return whether element's dependencies are met
  public function checkPropertyConditions($property) {
    // If property is set
    if (isset($this->properties[$property])) {
      // Evaluate the condition and return the result
      $result = \DF\evaluate_condition($this->properties[$property]);
    }
    else {
      $result = TRUE;
    }
    // Declare relevant hooks
    if ($property == '@dependencies') {
      $func = 'DF\check_dependencies';
    }
    // Check if hooks exist
    if (isset($func) AND is_callable($func)) {
      $arguments = array(
        'field' => $this->name,
        'result' => $result,
      );
      $hook_result = $func($arguments);
    }
    
    if (isset($hook_result)) {
      return $hook_result;
    }
    else {
      return $result;
    }
  }
  
  // Return whether element's dependencies are met
  public static function hook_set_attribute($name, $attribute, &$formArray, $default=NULL) {
    // If property is set
    if (isset($formArray[$attribute])) {
      // Evaluate the condition and return the result
      $result = $formArray[$attribute];
    }
    else {
      $result = $default;
    }
    // Declare relevant hooks
    if ($attribute == '#required') {
      $func = 'DF\check_required';
    }
    // Check if hooks exist
    if (isset($func) AND is_callable($func)) {
      $arguments = array(
        'field' => $name,
        'result' => $result,
      );
      $hook_result = $func($arguments);
    }
    
    if (isset($hook_result)) {
      $formArray[$attribute] = $hook_result;
    }
    elseif (isset($default) AND $default != NULL) {
      $formArray[$attribute] = $result;
    }
  }

  // Sets the attribute based on condition
  public function setAttribute($attribute, $property) {
    // If property is set
    if (isset($this->properties[$property])) {
      // Get value and/or condition
      $val_condition = $this->properties[$property];
      // if is condition
      if (is_array($val_condition)) {
        // Set attribute on the result of the condition
        $this->attributes[$attribute] = evaluate_condition($val_condition);
        /*
          if (!$this->attributes[$attribute]) {
          unset($this->attributes[$attribute]);
          }
         */
      }
      // If a value (or a static condition)
      else {
        // Set attribute to value (or static condition)
        $this->attributes[$attribute] = $val_condition;
      }
    }
  }

  // Get & Set Functions
  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    // Add inheritable name prefix if available
    if (isset($this->properties['@inherited_prefix'])) {
      $name = $this->properties['@inherited_prefix'] . $name;
    }
    // Add inheritable name suffix if available
    if (isset($this->properties['@inherited_suffix'])) {
      $name .= $this->properties['@inherited_suffix'];
    }
    
    // Convert Placeholders
    $name = convert_placeholders($name);
    
    // Reserve name
    //if (reserve_form_name($name)) {
    //  $name = Element::incrementName($name);
    //}
    // Save and store name
    $this->name = $name;
    $this->properties['@name'] = $name;
    //reserve_form_name($name);
  }

}

/* * ********************************************************************
 * Initialize session
 */

function init_storage() {
  // Init storage if not set
  if (!isset($_SESSION['dynamic_forms'])) {
    // Init main storage container
    $_SESSION['dynamic_forms'] = array();
    // Init defaul-value storage
    $_SESSION['dynamic_forms']['#default_values'] = array();
    // Init defaul-value storage
    $_SESSION['dynamic_forms']['#values'] = array();
    // Init forms storage
    $_SESSION['dynamic_forms']['#forms'] = array();
    // Init global variable storage
    $_SESSION['dynamic_forms']['#variables'] = array();
    // Init current element pointer
    $_SESSION['dynamic_forms']['#current_element'] = '';
    // Set form info storage
    $_SESSION['dynamic_forms']['#form_info'] = array();
    // Set submitted to false
    $_SESSION['dynamic_forms']['#form_info']['#submitted'] = FALSE;
  }
}

/**
 * Clear storage
 */
function clear_storage() {
  if (isset($_SESSION['dynamic_forms'])) {
    unset($_SESSION['dynamic_forms']);
  }
}

/**
 * Reset storage
 */
function reset_storage() {
  clear_storage();
  init_storage();
}

/* * ********************************************************************
 * Check if form object is set
 */

function check_form_object($elementName) {
  return isset($_SESSION['dynamic_forms']['#form_objects'][$elementName]);
}

/**
 * Get form object
 */
function get_form_object($elementName) {
  if (check_form_object($elementName)) {
    return unserialize($_SESSION['dynamic_forms']['#form_objects'][$elementName]);
  }
}

/**
 * Set form object
 */
function set_form_object(Element $elementObj) {
  // Verify storage is initialized
  init_storage();
  // Set form object
  return $_SESSION['dynamic_forms']['#form_objects'][$elementObj->name] = serialize($elementObj);
}

/* * ********************************************************************
 * Check whether a value has been submitted values
 */

function check_value($field) {
  // Clean field name if necessary
  if (isset($field[0]) AND $field[0] == '{' AND substr($field, -1) == '}') {
    $field = substr($field, 1, -1);
  }
  // Check if global variable is set
  return isset($_SESSION['dynamic_forms']['#values'][$field]);
}

/**
 * Get a submitted values
 */
function get_value($field) {
  // Clean field name if necessary
  if (isset($field[0]) AND $field[0] == '{' AND substr($field, -1) == '}') {
    $field = substr($field, 1, -1);
  }
  // If global variable if set
  if (check_value($field)) {
    return $_SESSION['dynamic_forms']['#values'][$field];
  }
  // If global variable is not found, return return default
  else {
    return get_default_value($field);
  }
}

/**
 * Set a submitted value
 */
function set_value($field, $value) {
  // Verify storage is initialized
  init_storage();
  // Clean field name if necessary
  if (isset($field[0]) AND $field[0] == '{' AND substr($field, -1) == '}') {
    $field = substr($field, 1, -1);
  }
  // Set global variable
  $_SESSION['dynamic_forms']['#values'][$field] = $value;
}

/* * ********************************************************************
 * Variable getters/setters
 */
function get_variable($name, &$elementObj) {
  if (isset($name[0]) AND $name[0] == '<' AND substr($name, -1) == '>') {
    return get_global_variable($name);
  }
  elseif (isset($name[0]) AND $name[0] == '[' AND substr($name, -1) == ']') {
    return get_local_variable($name, $elementObj);
  }
  // Default case
  else {
    return get_local_variable($name, $elementObj);
  }
}

function set_variable($name, $value, &$elementObj, $initialize=FALSE) {
  if (isset($name[0]) AND $name[0] == '<' AND substr($name, -1) == '>') {
    if ($initialize) {
      set_global_variable($name, $value);
    }
    else {
      init_global_variable($name, $value);
    }
  }
  elseif (isset($name[0]) AND $name[0] == '[' AND substr($name, -1) == ']') {
    set_local_variable($name, $value, $elementObj, $initialize);
  }
  // Default case
  else {
    set_local_variable($name, $value, $elementObj, $initialize);
  }
}



/* * ********************************************************************
 * Check whether a global variable has been set
 */

function check_global_variable($name) {
  // Clean variable if necessary
  if (isset($name[0]) AND $name[0] == '<' AND substr($name, -1) == '>') {
    $name = substr($name, 1, -1);
  }
  // Check if global variable is set
  return isset($_SESSION['dynamic_forms']['#variables'][$name]);
}

/**
 * Get a global global variable
 */
function get_global_variable($name) {
  // Clean variable if necessary
  if (isset($name[0]) AND $name[0] == '<' AND substr($name, -1) == '>') {
    $name = substr($name, 1, -1);
  }
  // If global variable if set
  if (check_global_variable($name)) {
    return $_SESSION['dynamic_forms']['#variables'][$name];
  }
}

/**
 * Set a global variable
 */
function set_global_variable($name, $value) {
  // Verify storage is initialized
  init_storage();
  // Clean variable if necessary
  if (isset($name[0]) AND $name[0] == '<' AND substr($name, -1) == '>') {
    $name = substr($name, 1, -1);
  }
  // Set global variable
  $_SESSION['dynamic_forms']['#variables'][$name] = $value;
}

/**
 * Initialize a global variable
 * 
 * Set value only if variable is not initialized
 */
function init_global_variable($name, $value) {
  // Check whether global variable does not exists
  if (!check_global_variable($name)) {
    // Set global variable
    set_global_variable($name, $value);
  }
}

/* * ********************************************************************
 * Get a local global variable
 */
function get_local_variable($var, &$element_obj) {
  // Get variable name
  $var_name = parse_local_variable_name($var);
  // Get the object scope of the variable
  $scoped_obj = get_variable_scope_object($var, $element_obj);
  // If found, return variable
  if (isset($scoped_obj)) {
    return $scoped_obj->variables[$var_name];
  }
}

/**
 * Set a local global variable
 */
function set_local_variable($var, $value, &$element_obj, $initialize=FALSE) {// Get variable name
  // Get variable name
  $var_name = parse_local_variable_name($var);
  // If set in the current scope if the variable is also to be initialized...
  if ($initialize) {
    $element_obj->variables[$var_name] = $value;
  }
  // Otherwise set in scope where it was declared (default)
  else {
    // Get the object scope of the variable
    $scoped_obj = get_variable_scope_object($var_name, $element_obj);
    // If found, set variable
    if (isset($scoped_obj)) {
      $scoped_obj->variables[$var_name] = $value;
    }
  }
  
}

/**
 * Parse variable name
 */
function parse_local_variable_name($var) {
  if (isset($var)) {
    // Remove brackets if necessary
    if (isset($var[0]) AND $var[0] == '[' AND substr($var, -1) == ']') {
      $var = substr($var, 1, -1);
      // Remove build time marker
      if (substr($var, 1, 2) == 'B:') {
        $var = substr($var, 2);
      }
    }
    
    // Find last position of a slash deliminator
    $pos = strrpos($var, "\\");
    // If not found, return whole string
    if ($pos === FALSE) {
      return $var;
    }
    // If found, then parse
    else {
      return substr($var, $pos+1);
    }
  }
}


/**
 * Parse variable name
 */
function parse_variable_relative_scope($var) {
  // Init scope path
  $scope_path = array();
  
  // Check if variable is set
  if (isset($var)) {
    // Remove brackets if necessary
    if (isset($var[0]) AND $var[0] == '[' AND substr($var, -1) == ']') {
      $var = substr($var, 1, -1);
      // Remove build time marker
      if (substr($var, 1, 2) == 'B:') {
        $var = substr($var, 2);
      }
    }
    
    $last_pos = 0;
    while (TRUE) {
      // Find position of first slash deliminator
      $cur_pos = strpos($var, "\\", $last_pos);
      // Break if not found
      if ($cur_pos === FALSE) {
        break;
      }
      // Add slash if in first position
      if ($cur_pos === 0) {
        $scope_path[] = "\\";
      }
      // Otherwise parse scope path element and add to scope path if valie
      else {
        // Parse scope path element
        $path_elem = substr($var, $last_pos, $cur_pos - $last_pos);
        // Add path
        //if ($path_elem == '..' OR ctype_alnum($path_elem)) {
        $scope_path[] = $path_elem;
        //}
      }
      // Set last position
      $last_pos = $cur_pos + 1;
    }
  }
  // Return scope path
  return $scope_path;
}

/**
 * Get variable scope
 */
function get_variable_scope_object($var, &$element_obj) {
  // Get variable name
  $var_name = parse_local_variable_name($var);
  // Get variable scope path
  $scope_path = parse_variable_relative_scope($var);
    
  if (!empty($element_obj)) {
    // Set current element
    $current_element =& $element_obj;
    // If variable scope is specified, start at it
    if (isset($scope_path)) {
      // Iterate over the scope elements
      foreach ($scope_path as $scope_elem) {
        // If absolute path is specified
        if ($scope_elem == "\\") {
          // Go to root element
          while($current_element->component_of) {
            // Set current element to parent element
            $current_element = &$current_element->component_of;
          }
        }
        // If parent scope specified
        elseif ($scope_elem == '..') {
          if (empty($current_element->component_of)) {
            // Path error
            return NULL;
          }
          else {
            // Set current element to parent element
            $current_element = &$current_element->component_of;
          }
        }
        else {
          // Go up a scope to a particular element
          $current_element = &$current_element->elements[$scope_elem];
        }
      }
    }
    
    // Iteratively go down element scopes
    while (TRUE) {
      // Check if the element has variables specified
      if (!empty($current_element->variables) AND is_array($current_element->variables)) {
        // Check if the element has the particular local variable
        if (isset($current_element->variables[$var_name])) {
          // Get local scope
          return $current_element;
        }
      }
      // Does the element have a parent...
      if (!empty($current_element->component_of)) {
        // Set current element to parent element
        $current_element = &$current_element->component_of;
      }
      // If no more parents and variable not found yet, then return NULL
      else {
        return NULL;
      }
    }
  }
}


/* * ********************************************************************
 * Check whether a forms system variable is available
 */

function check_system_variable($name) {
  // Add @ sign to denote system variable
  if (isset($name[0]) AND $name[0] != '@') {
    $name = '@' . $name;
  }
  // Check if variable is set
  return check_global_variable($name);
}

/**
 * Get a submitted values
 */
function get_system_variable($name) {
  // Add @ sign to denote system variable
  if (isset($name[0]) AND $name[0] != '@') {
    $name = '@' . $name;
  }
  // If variable if set
  return get_global_variable($name);
}

/**
 * Set a submitted value
 */
function set_system_variable($name, $value) {
  // Add @ sign to denote system variable
  if (isset($name[0]) AND $name[0] != '@') {
    $name = '@' . $name;
  }
  // Set variable
  set_global_variable($name, $value);
}

/* * ********************************************************************
 * Check whether default value has been set
 */

function check_default_value($field) {
  // Check whether default value exists
  return isset($_SESSION['dynamic_forms']['#default_values'][$field]);
}

/**
 * Get the default value of a field
 */
function get_default_value($field) {
  // If default value exists, return it
  if (check_default_value($field)) {
    return $_SESSION['dynamic_forms']['#default_values'][$field];
  }
}

/**
 * Set a default value
 */
function set_default_value($field, $value) {
  // Verify storage is initialized
  init_storage();
  // Set global variable
  $_SESSION['dynamic_forms']['#default_values'][$field] = $value;
}

/* * ********************************************************************
 * Check if element structure is set
 */

function check_element_structure($elementName) {
  return isset($_SESSION['dynamic_forms']['#forms'][$elementName]);
}

/**
 * Get renderable element structure
 */
function get_element_structure($elementName) {
  if (check_element_structure($elementName)) {
    return $_SESSION['dynamic_forms']['#forms'][$elementName];
  }
}

/**
 * Set renderable element structure
 */
function set_element_structure($elementName, $elementStructure) {
  // Verify storage is initialized
  init_storage();
  // Set element structure
  $_SESSION['dynamic_forms']['#forms'][$elementName] = $elementStructure;
}

/**
 * Reserve form name
 */
function check_form_name($name) {
  // Check if name has been created or reserved
  return check_element_structure($name);
}

/**
 * Reserve form name
 */
function reserve_form_name($name) {
  // Verify storage is initialized
  init_storage();
  // Set form object in not set
  if (!check_element_structure($name)) {
    $_SESSION['dynamic_forms']['#forms'][$name] = '';
  }
}

//*********************************************************************

/**
 * Set the current form URL for reference on AJAX calls
 */
function set_current_form($form_url) {
  $_SESSION['dynamic_forms']['#form_info']['#current_form_url'] = $form_url;
}

/**
 * Get the current form URL specified by the browser
 */
function get_current_form() {
  // Get form URL
  $form_url = current_path();

  // If AJAX call...
  if ($form_url == 'system/ajax') {
    // Replace URL with value previously stored
    $form_url = _get_current_saved_form();
  }
  // If not AJAX call...
  else {
    // Store form URL for later reference
    set_current_form($form_url);
  }

  // Return URL
  return $form_url;
}


/**
 * Get the current form URL for reference on AJAX calls
 */
function _get_current_saved_form() {
  if (isset($_SESSION['dynamic_forms']['#form_info']['#current_form_url'])) {
    return $_SESSION['dynamic_forms']['#form_info']['#current_form_url'];
  }
  else {
    return NULL;
  }
}


//*********************************************************************

/**
 * Get value based on condition
 */
function conditional_get_value($condition, $source_true, $source_false) {
  // Get the correct value
  $source = if_condition($condition, $source_true, $source_false);
  // Replace placeholder if necessary
  $source = convert_placeholders($source);
  // Return value
  return $source;
}

/**
 * Set value based on condition
 */
function conditional_set_value($target, $condition, $source_true, $source_false) {
  // Get the correct value
  $source = if_condition($condition, $source_true, $source_false);
  // Set value
  set_value($target, $source);
}

/**
 * Returns a value based on a condition
 */
function if_condition($condition, $value_true, $value_false) {
  // Check condition...
  // if true, return $value_true
  if (evaluate_condition($condition)) {
    return convert_placeholders($value_true);
  }
  // if false, return $value_false
  else {
    return convert_placeholders($value_false);
  }
}

/**
 * Parse array
 */

/**
 * Get any submitted values
 */
function evaluate_condition($condition) {

  // Get evaluation time
  $eval_time = get_system_variable('eval_time');
  // If structure evaluation time exists
  if (isset($condition['eval_time'])) {
    $struct_eval_time = $condition['eval_time'];
  }
  // If structure does not have specified evaluation time
  else {
    // Set structure evaluation time to default
    $struct_eval_time = 'R';
  }

  // Break if current evalutation time does not equal structure time
  if ($eval_time != $struct_eval_time) {
    return $condition;
  }

  // Check for non-conditional TRUE/FALSE
  if (!is_array($condition)) {
    $condition = convert_placeholders($condition);
    if ($condition == TRUE) {
      return TRUE;
    }
    elseif ($condition = FALSE) {
      return FALSE;
    }
  }
  else {
    // If callback condition
    if (isset($condition['@callback'])) {
      // Get function name
      $func = $condition['@callback'];
      // Check if callable
      $func_callable = is_callable($func);
      // If not callable, try adding the DF namespace if not specified
      if (!$func_callable AND substr($func, 0, 3) != "DF\\") {
        $func = "DF\\" . $func;
        $func_callable = is_callable($func);
      }
      // Call function if callable
      if ($func_callable) {
        if (isset($condition['@arguments'])) {
          return $func($condition['@arguments']);
        }
        else {
          return $func;
        }
      }
    }
    // Check for multi-condition
    elseif (isset($condition['multi-condition'])) {

      // Set multi-condition operator
      $operator = strtoupper(trim($condition['multi-condition']));
      if ($operator == '&&') {
        $operator = 'AND';
      }
      elseif ($operator == '||') {
        $operator = 'OR';
      }

      // Sets AND/OR flags
      $OR_flag = FALSE;
      $AND_flag = TRUE;
      // Iterate over the conditions
      foreach ($condition as $key => $subcondition) {

        // Set operator
        if ($key !== 'multi-condition' AND $key !== 'eval_time') {

          // Make sure subconditions have proper eval time set
          if (is_array($subcondition) AND isset($condition['eval_time'])) {
            $subcondition['eval_time'] = $condition['eval_time'];
          }

          // Evaluation subcondition
          $result = evaluate_condition($subcondition);
          // Set appropriate AND/OR flag
          if ($operator == 'AND') {
            $AND_flag = $result;
          }
          elseif ($operator == 'OR') {
            $OR_flag = $result;
          }
          // Check if AND/OR flags signal return
          if (!$AND_flag) {
            return FALSE;
          }
          if ($OR_flag) {
            return TRUE;
          }
        }
      }
      if ($operator == 'AND') {
        return $AND_flag;
      }
      elseif ($operator == 'OR') {
        return $OR_flag;
      }
    }
    // Single condition (if properly filled out
    elseif (isset($condition['value1']) AND isset($condition['operator'])) {
      // Get conditional components
      $value1 = $condition['value1'];
      $operator = $condition['operator'];
      
      if (isset($condition['value2'])) {
        $value2 = $condition['value2'];
      }
      else {
        $value2 = '';
      }

      // Check for subconditions
      if (is_array($value1)) {
        $value1 = evaluate_condition($value1);
      }
      if (is_array($value2)) {
        $value2 = evaluate_condition($value1);
      }

      // Check for field placeholders
      $value1 = convert_placeholders($value1);
      $operator = convert_placeholders($operator);
      $value2 = convert_placeholders($value2);

      // Check if a field has been selected
      if ($value1 != NULL) {

        // Check conditional statement
        if ($operator == 'any_value') {
          return TRUE;
        }
        elseif ($operator == 'no_value') {
          if ($value1 == '') {
            return TRUE;
          }
          else {
            return FALSE;
          }
        }
        elseif ($operator == 'is_in') {
          if (!empty($value2)) {
            return in_array($value1, $value2, true);
          }
          else {
            return empty($value1);
          }
        }
        elseif ($operator == 'is_not_in') {
          if (!empty($value2)) {
            return !in_array($value1, $value2, true);
          }
          else {
            return !empty($value1);
          };
        }
        elseif ($operator == '==') {
          return $value1 == $value2;
        }
        elseif ($operator == '!=') {
          return $value1 != $value2;
        }
        elseif ($operator == '<>') {
          return $value1 <> $value2;
        }
        elseif ($operator == '<') {
          return $value1 < $value2;
        }
        elseif ($operator == '>') {
          return $value1 > $value2;
        }
        elseif ($operator == '<=') {
          return $value1 <= $value2;
        }
        elseif ($operator == '>=') {
          return $value1 >= $value2;
        }
        elseif ((strtoupper($operator) == 'AND') OR ($operator == '&&')) {
          return $value1 AND $value2;
        }
        elseif ((strtoupper($operator) == 'OR') OR ($operator == '||')) {
          return $value1 OR $value2;
        }
        elseif (strtoupper($operator) == 'XOR') {
          return $value1 XOR $value2;
        }
        elseif ($operator == '&') {
          return $value1 & $value2;
        }
        elseif ($operator == '|') {
          return $value1 | $value2;
        }
      }
      // Field not submitted
      else {
        if ($operator == 'no_value') {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
    }
  }
}

/**
 * Analize array and return interpretted type
 */
function interpret_structure($structure) {
  // If not an array
  if (!is_array($structure)) {
    return convert_placeholders($structure);
  }
  else {
    // Single or hierarchical condition
    if (isset($structure['value1']) AND isset($structure['operator']) AND isset($structure['value2'])) {
      return evaluate_condition($structure);
    }
    // If callback condition
    elseif (isset($structure['@callback'])) {
      // Get function name
      $func = $structure['@callback'];
      // Check if callable
      $func_callable = is_callable($func);      
      // If not callable, try adding the DF namespace if not specified
      if (!$func_callable AND substr($func, 0, 3) != "DF\\") {
        $func = "DF\\" . $func;
        $func_callable = is_callable($func);
      }
      // Call function if callable
      if ($func_callable) {
        if (isset($structure['@arguments'])) {        
          return $func($structure['@arguments']);
        }
        else {
          return $func;
        }
      }
      // Not callable
      else {
        return '';
      }
    }
    // Multi-line condition
    elseif (isset($structure['multi-condition']) AND
        ( strtoupper($structure['multi-condition']) == 'AND' OR strtoupper($structure['multi-condition']) == 'OR' OR $structure['multi-condition'] == '&&' OR $structure['multi-condition'] == '||')) {
      return evaluate_condition($structure);
    }
    // Conditional get
    elseif (isset($structure['condition']) AND isset($structure['source_true']) AND isset($structure['source_false'])) {
      // Get evaluation time
      $eval_time = get_system_variable('eval_time');
      // If structure evaluation time exists
      if (isset($structure['eval_time'])) {
        $struct_eval_time = $structure['eval_time'];
      }
      // If condition evaluation exists
      elseif (isset($structure['condition']['eval_time'])) {
        $struct_eval_time = $structure['condition']['eval_time'];
      }
      // If structure does not have specified evaluation time
      else {
        // Set structure evaluation time to default
        $struct_eval_time = 'R';
      }

      // Return if current evalutation time does not equal structure time
      if ($eval_time != $struct_eval_time) {
        return $structure;
      }
      else {
        return conditional_get_value($structure['condition'], $structure['source_true'], $structure['source_false']);
      }
    }
    // Generate string
    elseif (isset($structure[0])) {
      $string = '';
      foreach ($structure as $value) {
        // Interpret structure
        $result = interpret_structure($value);
        // If $result was interpretable, then add it to $string
        if (is_string($result)) {
          $string .= $result;
        }
        // If $result not interpretable, likely because of delayed evaluation
        // then return the structure untouched
        else {
          return $structure;
        }
      }
      return $string;
    }
    // Unknown or invalid type
    else {
      return '';
    }
  }
}

/**
 * Renders FAPI element to HTML
 */
function render_to_html($elementName) {
  // Get Drupal Forms API structure
  $elementFAPI = get_element_structure($elementName);
  // If structure available...
  if (isset($elementFAPI)) {
    // If a container...
    if (isset($elementFAPI['#type']) AND $elementFAPI['#type'] == 'container') {
      return $elementFAPI['#prefix'] . $elementFAPI['#suffix'];
    }
    // If not a container
    else {
      // Get get submitted value or if not exists, get default value
      $value = get_value($elementName);

      // if $value has been previously submitted or setup, use it...
      if (isset($value) AND $value != NULL AND $value != '') {
        //$elementFAPI['#default_value'] = $value;
      }
      $html = drupal_render($elementFAPI);
      return $html;
    }
  }
  // If stucture not available
  else {
    return '';
  }
}

/**
 * Convert placeholders
 * 
 * Note: Variables have to be converted first, since if fields were converted
 *       first then the user could enter a global variable placeholder into the field
 *       to interfere with the system.
 */
function convert_placeholders($code) {
  
  // Check if it is array
  if (is_array($code)) {
    foreach ($code as &$item) {
      $item = convert_placeholders($item);
    }
  }
  // Check if string
  elseif (is_string($code)) {
    // Convert local variables
    $code = _parse_placeholders($code, '[', ']');
  }
  
  // Check if it is array
  if (is_array($code)) {
    foreach ($code as &$item) {
      $item = convert_placeholders($item);
    }
  }
  // Check if string
  elseif (is_string($code)) {
    // Convert global variables
    $code = _parse_placeholders($code, '<', '>');
  }
  // Check if it is array
  if (is_array($code)) {
    foreach ($code as &$item) {
      $item = convert_placeholders($item);
    }
  }
  // Check if string
  elseif (is_string($code)) {
    // Convert fields
    $code = _parse_placeholders($code, '{', '}');
  }  
  
  // Return converted string (or array)
  return $code;
}

/**
 * Parse placeholders
 */
function _parse_placeholders($string, $char_open, $char_close) {

  // Return parameter if not a string
  if (!is_string($string)) {
    return $string;
  }

  // Create a new array to hold parsed values
  $deliminated = array();
  // Holds current position
  $pos_start = 0;
  // Get evaluation-time
  $eval_time = get_system_variable('eval_time');

  //--------------------------------------------------------------
  // Parse string
  while (TRUE) {
    // Find location of opening character
    $pos_open = strpos($string, $char_open, $pos_start);
    // If opening character NOT found ...
    if ($pos_open === FALSE) {
      // Parse string until end of string
      $substr = substr($string, $pos_start);
      // Add parsed string if there is any
      if (strlen($substr) > 0) {
        $deliminated[] = $substr;
      }
      // Break because at end of string
      break;
    }
    //--------------------------------------------------------------
    // Opening character is found ...
    else {
      // Find closing character
      $pos_close = strpos($string, $char_close, $pos_open);
      // If closing character is NOT found
      if ($pos_close === FALSE) {
        // Parse until the end of the string
        $substr = substr($string, $pos_start);
        // Add parsed string if there is any
        if (strlen($substr) > 0) {
          $deliminated[] = $substr;
        }
        // Break because at end of string
        break;
      }
      //--------------------------------------------------------------
      // If BOTH opening and closing characters are found...
      else {
        // Set default evaluation-time
        $name_eval_time = 'R';
        // Determine evaluation-time (use default if not available)
        if (isset($string[$pos_open + 1]) AND isset($string[$pos_open + 2]) AND $string[$pos_open + 2] == ':') {
          $name_eval_time = strtoupper($string[$pos_open + 1]);
        }
        //--------------------------------------------------------------
        // Only parse if it is the correct evaluation-time
        If ($name_eval_time == $eval_time) {
          // Parse up until the opening character
          $substr = substr($string, $pos_start, $pos_open - $pos_start);
          // Add parsed string if there is one
          if (strlen($substr) > 0) {
            $deliminated[] = $substr;
          }
          // Parse placeholder
          $substr = substr($string, $pos_open, $pos_close - $pos_open + 1);
          // Replace placeholder and add it
          $deliminated[] = _replace_placeholder($substr);
          // Set start position just after the close position
          $pos_start = $pos_close + 1;
        }
        //--------------------------------------------------------------
        // If not the correct evaluation-time
        else {
          // Parse until the end of the string
          $substr = substr($string, $pos_start);
          // Add parsed string if there is any
          if (strlen($substr) > 0) {
            $deliminated[] = $substr;
          }
          // Break because at end of string
          break;
        }
      }
    }
  }
  // END LOOP
  //--------------------------------------------------------------
  $arr_flag = FALSE;
  foreach ($deliminated as $delim) {
    if (is_array($delim)) {
      // Return subarray if found
      return $delim;
    }
  }
  // Create new string and return it  
  return implode('', $deliminated);
  
}

/**
 * Replace the placeholder with appropriate value
 */
function _replace_placeholder($string) {

  // Return parameter if not a string
  if (!is_string($string)) {
    return $string;
  }

  // Make sure is large enough to have opening and closing brackets
  if (strlen($string) > 2) {

    // Check if it's a field
    if ($string[0] == '{' AND substr($string, -1) == '}') {
      $type = 'F';
    }
    // Check if it's a global variable
    elseif ($string[0] == '<' AND substr($string, -1) == '>') {
      $type = 'V';
    }
    // Check if it's a local variable
    elseif ($string[0] == '[' AND substr($string, -1) == ']') {
      $type = 'L';
    }

    // Parse name differently depending on whether evaluation-time (build/render) is specified
    if (strlen($string) > 4 AND (strtoupper($string[1]) == 'B' OR strtoupper($string[1]) == 'R') AND $string[2] == ':') {
      // Get placeholder name
      $parsed_name = substr($string, 3, -1);
    }
    // No evaluation-time specified
    else {
      // Get placeholder name
      $parsed_name = substr($string, 1, -1);
    }


    // Check for front space
    if ($parsed_name[0] == ' ') {
      $front_space = ' ';
    }
    else {
      $front_space = '';
    }
    // Check for back space
    if (substr($parsed_name, -1) == ' ') {
      $back_space = ' ';
    }
    else {
      $back_space = '';
    }

    // Trim spaces
    $trimmed_name = trim($parsed_name);

    // Verify that type is set and that there is a name
    if (isset($type) AND $trimmed_name != '') {
      // If it's type field
      If ($type == 'F') {
        // Get placeholder value
        $replaced_value = get_value($trimmed_name);
      }
      // If it's type global variable
      If ($type == 'V') {
        // Get placeholder value
        $replaced_value = get_global_variable($trimmed_name);
      }
      // If it's type local variable
      If ($type == 'L') {
        // Get current element object
        $current_element_obj = get_current_element();
        // Get placeholder value
        $replaced_value = get_local_variable($trimmed_name, $current_element_obj);
      }

      // If placeholder_name is set
      if (!empty($replaced_value) AND !is_array($replaced_value)) {
        // Parse string (allow empty space to be added to front and back(
        $replaced_value = $front_space . $replaced_value . $back_space;
      }
    }
  }

  // Return replaced placeholder
  return $replaced_value;
}


/**
 * Set the current element being processed
 */
function set_current_element(&$elementObj) {
  $_SESSION['dynamic_forms']['#current_element'] = $elementObj;
}

/**
 * Get the current element being processed
 */
function get_current_element() {
  if (isset($_SESSION['dynamic_forms']['#current_element'])) {
    return $_SESSION['dynamic_forms']['#current_element'];
  }
}

/**
 * Alias for the forms_list get() function
 *
 * Params: ($get_property, $filter_property, $filter_value)
 *   e.g., ('file'       , 'url'           , 'forms'      )
 * 
 * I.e. Return the value for the 'file' property of the first form that 
 *      has 'url' => 'forms'
 */
function get_form_value() {
  return call_user_func_array('DF\forms_list::get', func_get_args());
}
